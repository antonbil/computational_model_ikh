﻿using System;
using System.Collections.Generic;
using ConsoleApplication.Model;
using System.IO;
using System.Linq;
using ConsoleApplication.Algorithm;
using ConsoleApplication.Interface;

// ReSharper disable SuggestVarOrType_Elsewhere

/*
voor threads in python: https://hackernoon.com/synchronization-primitives-in-python-564f89fee732
activiteitA.join - betekent: ik wacht tot activiteitA klaar is. 
Als er meerdere joins actief zijn voor 1 activityC, zal die activityC moeten wachten tot al die activiteiten 
die genoemd worden in de join klaar zijn.
activityB.sync betekent: ik wacht totdat activityB klaar is / gestart is (nog even vragen aan Hans welke van die 
2 dan correct is) 
In python is de Thread.Barrier geschikt hiervoor.

 */
namespace ConsoleApplication
{
    internal static class Program
    {

        public static void Main(string[] args)
        {
            new ReadJson(@"resources/import.json").Read();
            DisplayAll();

            Console.WriteLine("End program");
            Condition c1 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 1 P1S2"));
            c1.Value = 0.5;
            //Console.WriteLine("c1:"+c1);
            Condition c2 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 2 P1S2"));
            c2.Value = -0.5;
            //Console.WriteLine("c2:"+c2);
            Algorithm.Algorithm a = new Algorithm.Algorithm();
            a.Execute("IKH Start P1S1");
            DisplayLog(a.LogQueue);
        }

        private static void DisplayLog(List<LogActivity> logQueue)
        {
            logQueue.ForEach(x =>
            {
                Console.WriteLine("execute:"+x.Activity.Id+(x.Log.Length>0?"\n    "+x.Log:""));
            });
        }

        private static void DisplayAll()
        {
            List<string> lines = new List<string>();
            void Title(string pre,string title)
            {
                lines.Add( pre + title + "\n" + string.Concat(Enumerable.Repeat("=", title.Length)));
            }
            
            Title("","Activities");
            Activity.Activities.ForEach(x=>lines.Add(x.ToString()));
            Title("\n","Identities");
            Identity.Identities.ForEach(x=>lines.Add(x.ToString()));
            Title("\n","Situations");
            Situation.Situations.ForEach(x=>lines.Add(x.ToString()));
            File.WriteAllLines("AllLines.txt", lines);
        }
    }
}