using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication.Model;

// ReSharper disable UnusedMember.Local

namespace ConsoleApplication.Algorithm
{
    public struct LogActivity
    {
        public LogActivity(string log, Activity currentThread)
        {
            Log = log;
            Activity = currentThread;
        }

        public string Log { get; set; }
        public Activity Activity { get; set; }
    }
    public class Algorithm
    {
        const double LowConstant = Double.MinValue;

        //Singleton StaticAlgorithm
        public static Algorithm StaticAlgorithm;

        //queue to contain all elements of threads
        private List<Activity> _threadList = new List<Activity>();

        public Algorithm()
        {
            StaticAlgorithm = this;
            // update status of activities to be able to check for Joining
            Activity.Activities.ForEach(x =>
            {
                // successor must check for Join
                x.Joins.ForEach(y =>
                {
                    y.NumJoins++;
                    y.TotalJoins=y.NumJoins;
                });
            });
            //initialize starting conditions for all clusters
            Activity.Activities.FindAll(y => y.Children.Count > 0).ForEach(x =>
            {
                //set FinishedCluster to deal with IOR/XOR-clusters
                x.FinishedCluster = false;
            });
        }

        /**
         * add successors of currentNode to list nextActs
         * also updates the threadlist, so activities will not be forgotten
         */
        private List<Activity> AddSuccessorsToList(List<Activity> nextActs, Activity currentNode)
        {
            //pars and joins to the tail
            List<Activity> appendList = currentNode.Pars.Union(currentNode.Joins).ToList();
            appendList.ForEach(x =>
            {
                nextActs.Add(x);
                AppendActivity(x);
            });
            //now add relations for compound activities
            //successors are added to front to be executed first
            //seqs (only one selected) to the front
            //only one seq can be chosen with the highest priority
            Activity seq = null;
            double highest = LowConstant;
            currentNode.Seqs.ForEach(currentSeq =>
            {
                double currentPriority = LowConstant + 1;
                currentNode.Conditions.ForEach(currentCondition =>
                {
                    if (currentCondition.Next.Id.Equals(currentSeq.Id))
                    {
                        currentPriority = new Model.Parser.ExpressionParser(@"" + currentCondition.Condition).EvaluateDouble();
                    }
                });
                if (!(currentPriority > highest)) return;
                highest = currentPriority;
                seq = currentSeq;
            });
            // if seq not null; no other relation in compound-activity can be selected
            if (seq == null) return nextActs;
            PrependActivity(seq);
            nextActs = nextActs.Prepend(seq).ToList();
            return nextActs;

        }

        //execute the algorithm
        public void Execute(string actionId)
        {
            LogQueue = new List<LogActivity>();
            int num = 0;
            // add first activity to start with
            Activity firstActivity = Activity.Activities.Find(x => x.Id.Equals(actionId));
            firstActivity.CurrentThread = new Thread(null);
            _threadList.Add(firstActivity);

            // start loop
            while (_threadList.Count > 0)
            {
                CleanThreadlist("start");
                // current thread
                Activity currentThread = _threadList.Select(x => x.Eligible().Activity1).FirstOrDefault(x => x != null);
                //remove element from Queue
                _threadList.Remove(currentThread);

                if (currentThread == null)
                {
                    //Console.WriteLine("thread null!!!!");
                    break;
                }

                currentThread.Mode = RunningMode.Running;
                while (currentThread.Mode == RunningMode.Running)
                {
                    CleanThreadlist("running");
                    //sanity check; won't be necessary....
                    num++;
                    if (num > 1000)
                    {
                        Environment.Exit(1);
                    }

                    string log = currentThread.Execute();
                    AddLog(currentThread, log);

                    currentThread.Joins.ForEach(x =>
                    {
                        //you could check if x in Joins of currentThread; but not necessary....
                        x.NumJoins--;
                        try
                        {
                            x.AddJoin(currentThread.CurrentThread.PreviousThread);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("error joining:"+x.Id);
                        }
                    });


                    //nextActs contains the list of executable nodes to follow
                    List<Activity> nextActs = new List<Activity>();
                    
                    nextActs = AddSuccessorsToList(nextActs, currentThread);
                    // iterate up to get next activities in parent
                    // do this until at root activity, or an eligible candidate found
                    Activity currentParent = currentThread;
                    //suggestion Rider does not look credible....
                    // ReSharper disable once MergeSequentialChecks
                    while (nextActs.Count == 0 && currentParent.PartOf != null &&
                           currentParent.PartOf is Activity activity && activity.Done)
                    {
                        //iterate up
                        currentParent = activity;
                        //add range, stop at the first available successor
                        nextActs = AddSuccessorsToList(nextActs, currentThread);
                    }
                    //discuss with Hans:
                    AddUpdatedPQRClustersToThread();

                    Activity nextThread = nextActs.Select(x => x.Eligible().Activity1).FirstOrDefault(x => x != null);
                    if (nextThread == null)
                    {
                        // (label: return) no next primitive available
                        // make sure the loop is stopped by changing the Mode of currentThread
                        currentThread.Mode = RunningMode.Active;
                        // only re-add if successors available
                        if (currentThread.Successors.Count != 0 && !_threadList.Contains(currentThread))
                            AppendActivity(currentThread);
                        break;
                    }

                    currentThread = nextThread;

                    if (currentThread.IsPQRCluster())
                    {
                        currentThread.Mode = RunningMode.Active;
                        _threadList.Add(currentThread);
                        break;
                    }

                    //if nextThread compound cluster:
                    if (currentThread.IsCompoundCluster())
                    {
                        //Hans states in his algorithm:
                        //currentThread.state := end state of current primitive
                        //and defines state as follows:
                        //state: refers to the state leading via (conditional) edges to possibly multiple primitives in
                        //(zero or more) PQR and (zero or one) compound cluster of which one will be executed in the end.
                        //
                        //imho. this end state of the current primitive is already checked.
                        //if in compound cluster: then using seq and alt-relations the next primitive is found.
                        //if there is no next primitive, the loop is stopped above using: if (nextThread == null)
                        //see: (label: return)
                        currentThread.Mode = RunningMode.Running;
                        //_threadList.Add(currentThread);
                    }

                }
            }
        }

        private void AddUpdatedPQRClustersToThread()
        {
            //Discuss this with Hans.
            //It is only relevant if conditions are defined anyway; so wait until model/implementation in wiki becomes richer
            //Changes in conditions might lead to triggering possibly more than one Q in a PQR cluster
            //So this one is executed to see for all pqr-clusters if p's have q's that become eligible (must be added to threadlist)
            //or on the contrary: are on the threadlist, but are not eligible anymore
            //It can be implemented to create a list of all separate pqr-clusters;
            //This is because only one leaf inside a pqr-cluster can be executed at once.
            //A node inside a cluster can be set active if one of its children is eligible.
            //NB. checking for this kinds of nodes only becomes relevant if no compound cluster is active; 
            // i.e. no seq/alt-activities are on thread
        }

        private void AddLog(Activity currentThread, string log)
        {
            LogQueue.Add(new LogActivity(log, currentThread));
        }

        public List<LogActivity> LogQueue { get; set; }

        /// <summary>
        /// clean up main thread-list; print list if asked to do so
        /// </summary>
        /// <param name="comment"></param>
        // ReSharper disable once UnusedParameter.Local
        private void CleanThreadlist(string comment)
        {
            //Console.WriteLine("Before:" + String.Join(",", _threadList.Select(x => x.Id)));
            _threadList = _threadList.FindAll(x => x != null && !x.Done);
            //Console.WriteLine(comment + ":" + String.Join(",", _threadList.Select(x => x.Id)));
        }

        /// <summary>
        /// helper function to get list of all successors
        /// </summary>
        /// <param name="a"></param>
        /// <param name="role"></param>
        /// <param name="successors"></param>
        /// <param name="level"></param>
        private void GetFirstSuccessors(Activity a, string role, List<Activity> successors, int level)
        {
            if (level > 50) return;
            if (a.Identity1 != null && !a.Identity1.Id.Equals(role)) return;
            if (a.Identity1 != null)
            {
                if (successors.Find(x => x.Id.Equals(a.Id)) == null)
                {
                    successors.Add(a);
                    //Console.WriteLine(a.Id);  
                }
            }
            else
            {
                a.Successors.ForEach(x => { GetFirstSuccessors(x, role, successors, level + 1); });
            }
        }

        private void GetFirstPredecessors(Activity a, string role, List<Activity> predecessors, int level)
        {
            if (level > 50) return;
            if (a.Identity1 != null && !a.Identity1.Id.Equals(role)) return;
            if (a.Identity1 != null && a.Identity1.Id.Equals(role))
                level++;
            if (a.Predecessors.Count == 0)
            {
                if (predecessors.Find(x => x.Id.Equals(a.Id)) == null && level > 0)
                {
                    predecessors.Add(a);
                    //Console.WriteLine(prev + "," + a.Id);
                }
            }
            else
            {
                a.Predecessors.ForEach(x =>
                {
                    //Console.WriteLine("get pred:"+x.Id);  
                    GetFirstPredecessors(x, role, predecessors, level);
                });
            }
        }


        
        private List<Activity> GetListOfActivitiesWithNoPredecessor(string role)
        {
            Activity.Activities.ForEach(x => x.Predecessors.Clear());
            Activity.Activities.ForEach(x =>
            {
                if (x.Identity1 == null || x.Identity1.Id.Equals(role))
                {
                    x.Successors.ForEach(y =>
                    {
                        if (x.Identity1 != null && x.Identity1.Id.Equals(role))
                        {
                            //Console.WriteLine(x.Id);
                        }

                        if (y.Predecessors.Find(z => z.Id.Equals(x.Id)) == null)
                        {
                            y.Predecessors.Add(x);
                        }
                    });
                }
            });
            // Activity.Activities.ForEach(x=>x.Successors.Clear());
            // Activity.Activities.ForEach(x =>
            // {
            //     if (x.Identity1 == null || x.Identity1.Id.Equals(role))
            //     {
            //         x.Predecessors.ForEach(y=>
            //         {
            //             if (y.Successors.Find(z => z.Id.Equals(x.Id)) == null)
            //                 y.Successors.Add(x);
            //         });
            //     }
            // });
            List<Activity> listOfActivitiesWithNoPredecessor =
                Activity.Activities.FindAll(x => x.Predecessors.Count > 0);

            List<Activity> ret = new List<Activity>();
            listOfActivitiesWithNoPredecessor.ForEach(x =>
            {
                //Console.WriteLine(x.Id);
                GetFirstPredecessors(x, role, ret, 0);
            });
            return ret;
        }

        /// <summary>
        /// prepend an activity before the current threadlist
        /// Only add this activity if is not already part of the threadlist
        /// </summary>
        /// <param name="activity"></param>
        // ReSharper disable once MemberCanBePrivate.Global
        public void PrependActivity(Activity activity)
        {
            if (_threadList.Exists(x => x.Id.Equals(activity.Id))) return;
            //Console.WriteLine("Prepend:"+activity.Id);
            _threadList = _threadList.Prepend(activity).ToList();
        }

        /// <summary>
        /// append an activity before the current threadlist
        /// Only add this activity if is not already part of the threadlist
        /// </summary>
        /// <param name="activity"></param>
        public void AppendActivity(Activity activity)
        {
            if (_threadList.Exists(x => x.Id.Equals(activity.Id))) return;
            //Console.WriteLine("Append:"+activity.Id);
            _threadList = _threadList.Append(activity).ToList();
        }
    }
}