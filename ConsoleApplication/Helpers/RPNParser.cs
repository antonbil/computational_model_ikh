
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

//code from: https://www.codeproject.com/Articles/5875/C-Expression-Parser-using-RPN
/*
 * Anton: improved, now handles functions, like exp and abs, and unary ! in logical expressions
 * Current Shortcomings
1.Unary operators (+,-) not handled.==> Anton: now handles !
2.Does not yet support expressions having mixture of variables and values (e.g. x+y/3*z%2 ).
3.Bitwise & and | not handled.
4.Does not handle a combo expression with multiple braces as just one of the ExpressionTypes?
// E.g. ((2+3)*2<10 || 1!=1) && 2*2==4 as just a logical expression cannot be done.
 */
/*
A typical usage of the parser would be as follows -

RPNParser parser = new RPNParser();

string szExpression = @"((2+3)*2<10 || 1!=1) && 2*2==4";

parser.EvaluateExpression( szExpression, 
    Type.GetType("System.Int64" ), false, null );
 */
namespace ConsoleApplication.Helpers
{
    public class RPNParser
    {
        /// <summary>
        /// The given expression can be parsed as either Arithmetic or Logical or 
        /// Comparison ExpressionTypes.  This is controlled by the enums 
        /// ExpressionType::ET_ARITHMETIC, ExpressionType::ET_COMPARISON and
        /// ExpressionType::ET_LOGICAL.  A combination of these enum types can also be given.
        /// E.g. To parse the expression as all of these, pass 
        /// ExpressionType.ET_ARITHMETIC|ExpressionType.ET_COMPARISON|ExpressionType.ET_LOGICAL 
        /// to the Tokenizer c'tor.
        /// </summary>
        [Flags]
        public enum ExpressionType
        {
            ET_ARITHMETIC = 0x0001,
            ET_COMPARISON = 0x0002,
            ET_LOGICAL = 0x0004
        }

        
        /// <summary>
        /// Currently not used.
        /// </summary>
        public enum TokenType
        {
            TT_OPERATOR,
            TT_OPERAND
        }

        public static bool IsApproximatelyEqualTo(double initialValue, double value, double maximumDifferenceAllowed)
        {
            // Handle comparisons of floating point values that may not be exactly the same
            return Math.Abs(initialValue - value) < maximumDifferenceAllowed;
        }

        public static bool IsApproximatelyEqualTo(double initialValue, double value)
        {
            return IsApproximatelyEqualTo(initialValue, value, 0.00001);
        }

        public object EvaluateExpression(string szExpr, Type varType, bool bFormula, Hashtable htValues)
        {
            ArrayList arrExpr = GetPostFixNotation(szExpr, varType, bFormula);
            return EvaluateRPN(arrExpr, varType, htValues);
        }

        public string Convert2String(ArrayList arrExpr)
        {
            string szResult = "";
            foreach (object obj in arrExpr) szResult += obj.ToString();

            return szResult;
        }


        #region RPN_Evaluator

        /// <summary>
        /// Algo of EvaluateRPN (source : Expression Evaluator : using RPN by lallous 
        /// in the C++/MFC section of www.CodeProject.com.
        /// 1.	Initialize stack for storing results, prepare input postfix (or RPN) expression. 
        ///	2.	Start scanning from left to right till we reach end of RPN expression 
        ///	3.	Get token, if token is: 
        ///		I.	An operator: 
        ///			a.	Get top of stack and store into variable op2; Pop the stack 
        ///			b.	Get top of stack and store into variable op1; Pop the stack 
        ///			c.	Do the operation expression in operator on both op1 and op2 
        ///			d.	Push the result into the stack 
        ///		II.	An operand: stack its numerical representation into our numerical stack. 
        ///	4.	At the end of the RPN expression, the stack should only have one value and 
        ///	that should be the result and can be retrieved from the top of the stack. 
        /// </summary>
        /// <param Name=szExpr>Expression to be evaluated in RPNotation with
        /// single character variables</param>
        /// <param Name=htValues>Values for each of the variables in the expression</param>
        /// 		
        public object EvaluateRPN(ArrayList arrExpr, Type varType, Hashtable htValues)
        {
            // initialize stack (integer stack) for results
            Stack stPad = new Stack();
            // begin loop : scan from left to right till end of RPN expression
            foreach (object var in arrExpr)
            {
                Operand op1 = null;
                Operand op2 = null;
                IOperator oprtr = null;
                // Get token
                // if token is 
                if (var is IOperand)
                {
                    // Operand : push onto top of numerical stack
                     stPad.Push(var);
                }
                else if (var is IOperator)
                {
                    // Operator :
                    // check if function; then it is operating on one term
                    if (functions.ContainsKey(var.ToString()))
                    {
                        // Operator :	
                        //		Pop top of stack 
                        op2 = (Operand) stPad.Pop();
                    
                        if (htValues != null) op2.Value = htValues[op2.Name];

                        //store new value in op2; operand it's value will change
                        
                        op2.Value = functions[var.ToString()](op2.Value);
                        //		Push results onto stack
                        stPad.Push(op2);
                        continue;

                    }
                    //		Pop top of stack into var 1 - op2 first as top of stack is rhs
                    op2 = (Operand) stPad.Pop();
                    
                    if (htValues != null) op2.Value = htValues[op2.Name];

                    //		Pop top of stack into var 2
                    try
                    {
                        op1 = (Operand)stPad.Pop();
                    }
#pragma warning disable 168
                    catch (Exception e)
#pragma warning restore 168
                    {
                        //if (var.Equals("-"))
                        {
                            //Console.WriteLine("var:" + var);
                            //Console.WriteLine("op2:" + op2.Value);
                            op2.Value = -(double)op2.Value;
                            //		Push results onto stack
                            stPad.Push(op2);
                            continue;
                        }
                    }

                    if (htValues != null) op1.Value = htValues[op1.Name];

                    //		Do operation exp for 'this' operator on var1 and var2
                    oprtr = (IOperator) var;
                    IOperand opRes = oprtr.Eval(op1, op2);
                    //		Push results onto stack
                    stPad.Push(opRes);
                }
            }

            //	end loop
            // stack ends up with single value with final result
            return ((Operand) stPad.Pop()).Value;
        }

        #endregion

        /// <summary>
        /// Represents each token in the expression
        /// </summary>
        private class Token
        {
            public Token(string szValue)
            {
                Value = szValue;
            }

            public string Value { get; }
        }

        /// <summary>
        /// Is the tokenizer which does the actual parsing of the expression.
        /// </summary>
        private class Tokenizer : IEnumerable
        {
            private readonly Regex _mRegEx;

            //string m_szExpression;
            private readonly string[] _mStrarrTokens;

            public Tokenizer(string szExpression, ExpressionType exType = ExpressionType.ET_ARITHMETIC |
                                                                          ExpressionType.ET_COMPARISON |
                                                                          ExpressionType.ET_LOGICAL)
            {
                //m_szExpression = szExpression;
                _mRegEx = new Regex(OperatorHelper.GetOperatorsRegEx(exType));
                _mStrarrTokens = SplitExpression(szExpression);
            }

            public IEnumerator GetEnumerator()
            {
                return new TokenEnumerator(_mStrarrTokens);
            }

            private string[] SplitExpression(string szExpression)
            {
                return _mRegEx.Split(szExpression);
            }
        }

        /// <summary>
        /// Enumerator to enumerate over the tokens.
        /// </summary>
        private class TokenEnumerator : IEnumerator
        {
            private readonly string[] _mStrarrTokens;
            private int _mNIdx;
            private Token _mToken;

            public TokenEnumerator(string[] strarrTokens)
            {
                _mStrarrTokens = strarrTokens;
                Reset();
            }

            public object Current => _mToken;

            public bool MoveNext()
            {
                if (_mNIdx >= _mStrarrTokens.Length)
                    return false;

                _mToken = new Token(_mStrarrTokens[_mNIdx]);
                _mNIdx++;
                return true;
            }

            public void Reset()
            {
                _mNIdx = 0;
            }
        }

        /// <summary>
        /// For the exceptions thrown by this module.
        /// </summary>
        private class RPN_Exception : ApplicationException
        {
            public RPN_Exception()
            {
            }

            public RPN_Exception(string szMessage) : base(szMessage)
            {
            }

            public RPN_Exception(string szMessage, Exception innerException) : base(szMessage, innerException)
            {
            }
        }

        public interface IOperand
        {}

        private interface IOperator
        {
            IOperand Eval(IOperand lhs, IOperand rhs);
        }

        private interface IArithmeticOperations
        {
            // to support {"+", "-", "*", "/", "%"} operators
            IOperand Plus(IOperand rhs);
            IOperand Minus(IOperand rhs);
            IOperand Multiply(IOperand rhs);
            IOperand Divide(IOperand rhs);
            IOperand Modulo(IOperand rhs);
        }

        private interface IComparisonOperations
        {
            // to support {"==", "!=","<", "<=", ">", ">="} operators
            IOperand EqualTo(IOperand rhs);
            IOperand NotEqualTo(IOperand rhs);
            IOperand LessThan(IOperand rhs);
            IOperand LessThanOrEqualTo(IOperand rhs);
            IOperand GreaterThan(IOperand rhs);
            IOperand GreaterThanOrEqualTo(IOperand rhs);
        }

        private interface ILogicalOperations
        {
            // to support {"||", "&&" } operators
            IOperand OR(IOperand rhs);
            IOperand AND(IOperand rhs);
        }

        /// <summary>
        /// Base class for all Operands.  Provides datastorage
        /// </summary>
        public abstract class Operand : IOperand
        {
            protected string MSzVarName = "";
            protected object MVarValue;

            protected Operand(string szVarName, object varValue)
            {
                MSzVarName = szVarName;
                MVarValue = varValue;
            }

            protected Operand(string szVarName)
            {
                MSzVarName = szVarName;
            }

            public string Name
            {
                get => MSzVarName;
                set => MSzVarName = value;
            }

            public object Value
            {
                get => MVarValue;
                set => MVarValue = value;
            }

            public override string ToString()
            {
                return MSzVarName;
            }

            public abstract void ExtractAndSetValue(string szValue, bool bFormula);
        }

        /// <summary>
        /// Operand corresponding to the Double datatype.
        /// </summary>
        private class DoubleOperand : Operand, IArithmeticOperations, IComparisonOperations
        {
            public DoubleOperand(string szVarName, object varValue) : base(szVarName, varValue)
            {
            }

            public DoubleOperand(string szVarName) : base(szVarName)
            {
            }

            /// IArithmeticOperations methods.  Return of these methods is again a Double
            public IOperand Plus(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.Plus : rhs");
                DoubleOperand oprResult = new DoubleOperand("Result", Type.GetType("System.Double"))
                {
                    Value = (double) Value + (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand Minus(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.Minus : rhs");
                DoubleOperand oprResult = new DoubleOperand("Result", Type.GetType("System.Double"))
                {
                    Value = (double) Value - (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand Multiply(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new ArgumentException("Argument invalid in Double.Multiply : rhs");
                DoubleOperand oprResult = new DoubleOperand("Result", Type.GetType("System.Double"))
                {
                    Value = (double) Value * (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand Divide(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.Divide : rhs");
                DoubleOperand oprResult = new DoubleOperand("Result", Type.GetType("System.Double"))
                {
                    Value = (double) Value / (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand Modulo(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.Modulo : rhs");
                DoubleOperand oprResult = new DoubleOperand("Result", Type.GetType("System.Double"));
                oprResult.Value = (double) Value % (double) ((Operand) rhs).Value;
                return oprResult;
            }

            /// IComparisonOperators methods.  Return values are always BooleanOperands type
            public IOperand EqualTo(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.== : rhs");
                //Console.WriteLine("check equal:"+Value +":"+((Operand) rhs).Value);
                //Console.WriteLine("check equal:"+((double)Value));
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = IsApproximatelyEqualTo((double) Value, (double) ((Operand) rhs).Value)
                };

                return oprResult;
            }

            public IOperand NotEqualTo(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.!= : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = !IsApproximatelyEqualTo((double) Value, (double) ((Operand) rhs).Value)
                };
                return oprResult;
            }

            public IOperand LessThan(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Decimal.< : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (double) Value < (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand LessThanOrEqualTo(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.<= : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (double) Value <= (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand GreaterThan(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.> : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (double) Value > (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public IOperand GreaterThanOrEqualTo(IOperand rhs)
            {
                if (!(rhs is DoubleOperand))
                    throw new RPN_Exception("Argument invalid in Double.>= : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (double) Value >= (double) ((Operand) rhs).Value
                };
                return oprResult;
            }

            public override string ToString()
            {
                return MSzVarName;
            }

            public override void ExtractAndSetValue(string szValue, bool bFormula)
            {
                // check if multiple parameters
                if (szValue.Contains(","))
                {
                    //multiple parameters, split using ","
                    try
                    {
                        //split using , and create arraylist with doubles if all elements are doubles
                        double[] values = szValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(s => double.Parse(s)).ToArray();
                        Object a = values;
                        MVarValue = !bFormula ? a : 0.0;
                    }
#pragma warning disable 168
                    catch (Exception e)
#pragma warning restore 168
                    {
                        try
                        {
                            //split using , and create arraylist with strings as fallback
                            string[] values = szValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                .ToArray();
                            Object a = values;
                            Console.WriteLine("convert to string array:"+values[0]+":" + values[1]);
                            MVarValue = !bFormula ? a : 0.0;
                        }
#pragma warning disable 168
                        catch (Exception e1)
#pragma warning restore 168
                        {
                            MVarValue = !bFormula ? Convert.ToDouble(szValue) : 0.0;
                        }
//                        MVarValue = !bFormula ? Convert.ToDouble(szValue) : 0.0;
                    }
                }
                else
                {
                    double d = 0.0;
                    if (!bFormula)
                    {
                        bool min = false;
                        if (szValue.StartsWith("_")||szValue.StartsWith("-"))
                        {
                            //deal with unary minus: it is replaced with _ Now apply it.
                            szValue = szValue.Remove(0, 1);
                            min = true;
                        }

                        d = Convert.ToDouble(szValue);
                        if (min)
                        {
                            d = -d;
                        }
                    }

                    MVarValue = d;
                }
            }
        }

        /// <summary>
        /// Operand corresponding to the Long (Int32/Int64) datatypes.
        /// </summary>
        private class LongOperand : Operand, IArithmeticOperations, IComparisonOperations
        {
            public LongOperand(string szVarName, object varValue) : base(szVarName, varValue)
            {
            }

            public LongOperand(string szVarName) : base(szVarName)
            {
            }

            /// IArithmeticOperations methods.  Return of these methods is again a LongOperand
            public IOperand Plus(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.Plus : rhs");
                LongOperand oprResult = new LongOperand("Result", Type.GetType("System.Int64"));
                oprResult.Value = (long) Value + (long) ((Operand) rhs).Value;
                return oprResult;
            }

            public IOperand Minus(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.Minus : rhs");
                LongOperand oprResult = new LongOperand("Result", Type.GetType("System.Int64"));
                oprResult.Value = (long) Value - (long) ((Operand) rhs).Value;
                return oprResult;
            }

            public IOperand Multiply(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new ArgumentException("Argument invalid in LongOperand.Multiply : rhs");
                LongOperand oprResult = new LongOperand("Result", Type.GetType("System.Int64"));
                oprResult.Value = (long) Value * (long) ((Operand) rhs).Value;
                return oprResult;
            }

            public IOperand Divide(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.Divide : rhs");
                LongOperand oprResult = new LongOperand("Result", Type.GetType("System.Int64"));
                oprResult.Value = (long) Value / (long) ((Operand) rhs).Value;
                return oprResult;
            }

            public IOperand Modulo(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.Modulo : rhs");
                LongOperand oprResult = new LongOperand("Result", Type.GetType("System.Int64"));
                oprResult.Value = (long) Value % (long) ((Operand) rhs).Value;
                return oprResult;
            }

            /// IComparisonOperators methods.  Return values are always BooleanOperands type
            public IOperand EqualTo(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.== : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (long) Value == (long) ((Operand) rhs).Value;
                return oprResult;
            }

            public IOperand NotEqualTo(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.!= : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (long) Value != (long) ((Operand) rhs).Value ? true : false;
                return oprResult;
            }

            public IOperand LessThan(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.< : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (long) Value < (long) ((Operand) rhs).Value ? true : false;
                return oprResult;
            }

            public IOperand LessThanOrEqualTo(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.<= : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (long) Value <= (long) ((Operand) rhs).Value ? true : false;
                return oprResult;
            }

            public IOperand GreaterThan(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.> : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (long) Value > (long) ((Operand) rhs).Value ? true : false
                };
                return oprResult;
            }

            public IOperand GreaterThanOrEqualTo(IOperand rhs)
            {
                if (!(rhs is LongOperand))
                    throw new RPN_Exception("Argument invalid in LongOperand.>= : rhs");
                BoolOperand oprResult = new BoolOperand("Result")
                {
                    Value = (long) Value >= (long) ((Operand) rhs).Value ? true : false
                };
                return oprResult;
            }

            public override string ToString()
            {
                return MSzVarName;
            }

            public override void ExtractAndSetValue(string szValue, bool bFormula)
            {
                MVarValue = !bFormula ? Convert.ToInt64(szValue) : 0;
            }
        }

        /// <summary>
        /// Operand corresponding to Boolean Type
        /// </summary>
        private class BoolOperand : Operand, ILogicalOperations
        {
            public BoolOperand(string szVarName, object varValue) : base(szVarName, varValue)
            {
            }

            public BoolOperand(string szVarName) : base(szVarName)
            {
            }

            public IOperand AND(IOperand rhs)
            {
                if (!(rhs is BoolOperand))
                    throw new RPN_Exception("Argument invalid in BoolOperand.&& : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (bool) Value && (bool) ((Operand) rhs).Value ? true : false;
                return oprResult;
            }

            public IOperand OR(IOperand rhs)
            {
                if (!(rhs is BoolOperand))
                    throw new RPN_Exception("Argument invalid in BoolOperand.|| : rhs");
                BoolOperand oprResult = new BoolOperand("Result");
                oprResult.Value = (bool) Value || (bool) ((Operand) rhs).Value ? true : false;
                return oprResult;
            }

            public override string ToString()
            {
                return Value.ToString();
            }

            public override void ExtractAndSetValue(string szValue, bool bFormula)
            {
                MVarValue = !bFormula && Convert.ToBoolean(szValue);
            }
        }

        private class OperandHelper
        {
            /// <summary>
            /// Factory method to create corresponding Operands.
            /// Extended this method to create newer datatypes.
            /// </summary>
            /// <param Name="szVarName"></param>
            /// <param Name="varType"></param>
            /// <param Name="varValue"></param>
            /// <returns></returns>
            public static Operand CreateOperand(string szVarName, Type varType, object varValue)
            {
                Operand oprResult = null;
                //Console.WriteLine("ev1:"+varType+":"+szVarName+":"+varValue);
                switch (varType.ToString())
                {
                    case "System.Int32":
                    case "System.Int64":
                        oprResult = new LongOperand(szVarName, varValue);
                        return oprResult;
                    case "System.Double":
                        //case System.Single:
                        oprResult = new DoubleOperand(szVarName, varValue);
                        return oprResult;
                    case "System.String":
                        //case System.Single:
                        Console.WriteLine("ev:"+szVarName+":"+varValue);
                        oprResult = new DoubleOperand(szVarName, varValue);
                        return oprResult;
                    //break;
                }

                throw new RPN_Exception("Unhandled type : " + varType);
            }

            public static Operand CreateOperand(string szVarName, Type varType)
            {
                return CreateOperand(szVarName, varType, null);
            }
        }

        /// <summary>
        /// Base class of all operators.  Provides datastorage
        /// </summary>
        private abstract class Operator : IOperator
        {
            protected string m_szOperator = "";

            public Operator(char cOperator)
            {
                m_szOperator = new string(cOperator, 1);
            }

            public Operator(string szOperator)
            {
                m_szOperator = szOperator;
            }

            public string Value
            {
                get => m_szOperator;
                set => m_szOperator = value;
            }

            public abstract IOperand Eval(IOperand lhs, IOperand rhs);

            public override string ToString()
            {
                return m_szOperator;
            }
        }

        /// <summary>
        /// Arithmetic Operator Class providing evaluation services for "+-/*%" operators.
        /// </summary>
        private class ArithmeticOperator : Operator
        {
            public ArithmeticOperator(char cOperator) : base(cOperator)
            {
            }

            public ArithmeticOperator(string szOperator) : base(szOperator)
            {
            }
            //bool bBinaryOperator = true;

            public override IOperand Eval(IOperand lhs, IOperand rhs)
            {
                if (!(lhs is IArithmeticOperations))
                    throw new RPN_Exception("Argument invalid in ArithmeticOperator.Eval - Invalid Expression : lhs");
                switch (m_szOperator)
                {
                    case "+":
                        return ((IArithmeticOperations) lhs).Plus(rhs);
                    case "-":
                        return ((IArithmeticOperations) lhs).Minus(rhs);
                    case "*":
                        return ((IArithmeticOperations) lhs).Multiply(rhs);
                    case "/":
                        return ((IArithmeticOperations) lhs).Divide(rhs);
                    case "%":
                        return ((IArithmeticOperations) lhs).Modulo(rhs);
                }

                throw new RPN_Exception("Unsupported Arithmetic operation " + m_szOperator);
            }
        }

        /// <summary>
        /// Comparison Operator Class providing evaluation services for "==", "!=","<", "<=", ">", ">=" operators.
        /// </summary>
        private class ComparisonOperator : Operator
        {
            public ComparisonOperator(char cOperator) : base(cOperator)
            {
            }

            public ComparisonOperator(string szOperator) : base(szOperator)
            {
            }
            //bool bBinaryOperator = true;

            //{"==", "!=","<", "<=", ">", ">="}
            public override IOperand Eval(IOperand lhs, IOperand rhs)
            {
                if (!(lhs is IComparisonOperations))
                    throw new RPN_Exception("Argument invalid in ComparisonOperator.Eval - Invalid Expression : lhs"+lhs+":"+rhs);
                switch (m_szOperator)
                {
                    case "==":
                        return ((IComparisonOperations) lhs).EqualTo(rhs);
                    case "!=":
                        return ((IComparisonOperations) lhs).NotEqualTo(rhs);
                    case "<":
                        return ((IComparisonOperations) lhs).LessThan(rhs);
                    case "<=":
                        return ((IComparisonOperations) lhs).LessThanOrEqualTo(rhs);
                    case ">":
                        return ((IComparisonOperations) lhs).GreaterThan(rhs);
                    case ">=":
                        return ((IComparisonOperations) lhs).GreaterThanOrEqualTo(rhs);
                }

                throw new RPN_Exception("Unsupported Comparison operation " + m_szOperator);
            }
        }

        /// <summary>
        /// Logical Operator Class providing evaluation services for && and || operators.
        /// </summary>
        private class LogicalOperator : Operator
        {
            public LogicalOperator(char cOperator) : base(cOperator)
            {
            }

            public LogicalOperator(string szOperator) : base(szOperator)
            {
            }
            //bool bBinaryOperator = true;

            //{"&&", "||"}
            public override IOperand Eval(IOperand lhs, IOperand rhs)
            {
                //Console.WriteLine("IOperand Eval:"+lhs+":"+rhs+":"+m_szOperator);
                //workaround; improve this to deal with doubles, and double-array-->should return lhs
                string shsString = "" + rhs;
                if (shsString.Contains(","))
                {
                    return lhs;
                }
                if (!(lhs is ILogicalOperations))
                    throw new RPN_Exception("Argument invalid in LogicalOperator.Eval - Invalid Expression : lhs");
                switch (m_szOperator)
                {
                    case "&&":
                        // return True if rhs is number or combination of numbers
                        return ((ILogicalOperations) lhs).AND(rhs);
                    case "||":
                        return ((ILogicalOperations) lhs).OR(rhs);
                }

                throw new RPN_Exception("Unsupported Logical operation " + m_szOperator);
            }
        }

        private class OperatorHelper
        {
            private static readonly string[] m_AllArithmeticOps = {"+", "-", "*", "/", "%"};
            private static readonly string[] m_AllComparisonOps = {"==", "!=", "<", "<=", ">", ">="};
            private static readonly string[] m_AllLogicalOps = {"&&", "||"};

            /// <summary>
            /// All Operators supported by this module currently.
            /// Modify here to add more operators IN ACCORDANCE WITH their precedence.
            /// Additionally add into individual variables to support some helper methods above.
            /// </summary>
            public static string[] AllOperators { get; } =
            {
                "_","!","||", "&&", "|", "^", "&", "==", "!=",
                "<", "<=", ">", ">=", "+", "-", "*", "/", "%", "(", ")"
            };

            /// <summary>
            /// Factory method to create Operator objects.
            /// </summary>
            /// <param Name="szOperator"></param>
            /// <returns></returns>
            public static IOperator CreateOperator(string szOperator)
            {
                IOperator oprtr = null;
                if (IsArithmeticOperator(szOperator)|| functions.ContainsKey(szOperator))
                {
                    oprtr = new ArithmeticOperator(szOperator);
                    return oprtr;
                }

                if (IsComparisonOperator(szOperator))
                {
                    oprtr = new ComparisonOperator(szOperator);
                    return oprtr;
                }

                if (IsLogicalOperator(szOperator))
                {
                    oprtr = new LogicalOperator(szOperator);
                    return oprtr;
                }

                throw new RPN_Exception("Unhandled Operator : " + szOperator);
            }

            public static IOperator CreateOperator(char cOperator)
            {
                return CreateOperator(new string(cOperator, 1));
            }

            /// Some helper functions.
            public static bool IsOperator(string currentOp)
            {
                int nPos = Array.IndexOf(AllOperators, currentOp.Trim());
                if (nPos != -1)
                    return true;
                return false;
            }

            public static bool IsArithmeticOperator(string currentOp)
            {
                int nPos = Array.IndexOf(m_AllArithmeticOps, currentOp);
                if (nPos != -1)
                    return true;
                return false;
            }

            public static bool IsComparisonOperator(string currentOp)
            {
                int nPos = Array.IndexOf(m_AllComparisonOps, currentOp);
                if (nPos != -1)
                    return true;
                return false;
            }

            public static bool IsLogicalOperator(string currentOp)
            {
                int nPos = Array.IndexOf(m_AllLogicalOps, currentOp);
                if (nPos != -1)
                    return true;
                return false;
            }

            #region Precedence

            /// Precedence is determined by relative indices of the operators defined in 
            /// in m_AllOps variable
            /// <summary>
            /// Summary of IsLowerPrecOperator.
            /// </summary>
            /// <param Name=allOps></param>
            /// <param Name=currentOp></param>
            /// <param Name=prevOp></param>
            /// 		
            public static bool IsLowerPrecOperator(string currentOp, string prevOp)
            {
                int nCurrIdx;
                int nPrevIdx;
                GetCurrentAndPreviousIndex(AllOperators, currentOp, prevOp, out nCurrIdx, out nPrevIdx);
                if (nCurrIdx < nPrevIdx) return true;

                return false;
            }

            /// <summary>
            /// Summary of IsHigherPrecOperator.
            /// </summary>
            /// <param Name=allOps></param>
            /// <param Name=currentOp></param>
            /// <param Name=prevOp></param>
            /// 		
            public static bool IsHigherPrecOperator(string currentOp, string prevOp)
            {
                int nCurrIdx;
                int nPrevIdx;
                GetCurrentAndPreviousIndex(AllOperators, currentOp, prevOp, out nCurrIdx, out nPrevIdx);
                if (nCurrIdx > nPrevIdx) return true;

                return false;
            }

            /// <summary>
            /// Summary of IsEqualPrecOperator.
            /// </summary>
            /// <param Name=allOps></param>
            /// <param Name=currentOp></param>
            /// <param Name=prevOp></param>
            /// 		
            public static bool IsEqualPrecOperator(string currentOp, string prevOp)
            {
                int nCurrIdx;
                int nPrevIdx;
                GetCurrentAndPreviousIndex(AllOperators, currentOp, prevOp, out nCurrIdx, out nPrevIdx);
                if (nCurrIdx == nPrevIdx) return true;

                return false;
            }

            /// <summary>
            /// Summary of GetCurrentAndPreviousIndex.
            /// </summary>
            /// <param Name=allOps></param>
            /// <param Name=currentOp></param>
            /// <param Name=prevOp></param>
            /// <param Name=nCurrIdx></param>
            /// <param Name=nPrevIdx></param>
            /// 		
            private static void GetCurrentAndPreviousIndex(string[] allOps, string currentOp, string prevOp,
                out int nCurrIdx, out int nPrevIdx)
            {
                nCurrIdx = -1;
                nPrevIdx = -1;
                for (int nIdx = 0; nIdx < allOps.Length; nIdx++)
                {
                    if (allOps[nIdx] == currentOp) nCurrIdx = nIdx;

                    if (allOps[nIdx] == prevOp) nPrevIdx = nIdx;

                    if (nPrevIdx != -1 && nCurrIdx != -1) break;
                }

                if (nCurrIdx == -1) throw new RPN_Exception("Unknown operator - " + currentOp);

                if (nPrevIdx == -1) throw new RPN_Exception("Unknown prev operator - " + prevOp);
            }

            #endregion

            #region RegEx

            /// <summary>
            /// This gets the regular expression used to find operators in the input
            /// expression.
            /// </summary>
            /// <param Name="exType"></param>
            /// <returns></returns>
            public static string GetOperatorsRegEx(ExpressionType exType)
            {
                StringBuilder strRegex = new StringBuilder();
                if ((exType & ExpressionType.ET_ARITHMETIC).Equals(ExpressionType.ET_ARITHMETIC))
                {
                    if (strRegex.Length == 0)
                        strRegex.Append(m_szArthmtcRegEx);
                    else
                        strRegex.Append("|" + m_szArthmtcRegEx);
                }

                if ((exType & ExpressionType.ET_COMPARISON).Equals(ExpressionType.ET_COMPARISON))
                {
                    if (strRegex.Length == 0)
                        strRegex.Append(m_szCmprsnRegEx);
                    else
                        strRegex.Append("|" + m_szCmprsnRegEx);
                }

                if ((exType & ExpressionType.ET_LOGICAL).Equals(ExpressionType.ET_LOGICAL))
                {
                    if (strRegex.Length == 0)
                        strRegex.Append(m_szLgclRegEx);
                    else
                        strRegex.Append("|" + m_szLgclRegEx);
                }

                if (strRegex.Length == 0)
                    throw new RPN_Exception("Invalid combination of ExpressionType value");
                return "(" + strRegex + ")";
            }

            /// <summary>
            /// Expression to pattern match various operators
            /// </summary>
            private static readonly string m_szArthmtcRegEx = @"[+\-*/%()]{1}";

            private static readonly string m_szCmprsnRegEx = @"[=<>!]{1,2}";
            private static readonly string m_szLgclRegEx = @"[&|]{2}";

            #endregion
        }

        #region RPN_Parser

        //list of all functions that are recognized by the parser
        private static readonly Dictionary<string, Func<object, object>> functions = new Dictionary<string, Func<object, object>>
        {
            ["exp"] = x=>  Math.Exp((double)x),
            ["sqrt"] = x=>  Math.Sqrt((double)x),
            ["sqr"] = x=>  (double)x * (double)x,
            //["ln"] = (x)=>  Math.Log(x),
            ["sin"] = x=>  Math.Sin((double)x),
            ["cos"] = x=>  Math.Cos((double)x),
            ["abs"] = x=>  Math.Abs((double)x),

            //! = not, the only logical function
            ["!"] = x=>  !(bool)x,
            ["_"] = x=>  -(double)x
        };

        /// <summary>
        /// Algo of GetPostFixNotation (source : Expression Evaluator : using RPN by lallous 
        /// in the C++/MFC section of www.CodeProject.com.
        /// 1.	Initialize an empty stack (string stack), prepare input infix expression and clear RPN string 
        ///	2.	Repeat until we reach end of infix expression 
        ///		I.	Get token (operand or operator); skip white spaces 
        ///		II.	If token is: 
        ///			a.	Left parenthesis: Push it into stack 
        ///			b.	Right parenthesis: Keep popping from the stack and appending to 
        ///				RPN string until we reach the left parenthesis.
        ///				If stack becomes empty and we didn't reach the left parenthesis 
        ///				then break out with error "Unbalanced parenthesis" 
        ///			c.	Operator: If stack is empty or operator has a higher precedence than 
        ///				the top of the stack then push operator into stack. 
        ///				Else if operator has lower precedence then we keep popping and 
        ///				appending to RPN string, this is repeated until operator in stack 
        ///				has lower precedence than the current operator. 
        ///			d.	An operand: we simply append it to RPN string. 
        ///		III.	When the infix expression is finished, we start popping off the stack and 
        ///				appending to RPN string till stack becomes empty. 
        ///
        /// </summary>
        /// <param Name=szExpr></param>
        /// 		
        public ArrayList GetPostFixNotation(string szExpr, Type varType, bool bFormula)
        {
            Stack stkOp = new Stack();
            ArrayList arrFinalExpr = new ArrayList();
            string szResult = "";

            Tokenizer tknzr = new Tokenizer(szExpr);
            foreach (Token token in tknzr)
            {
                string szToken = token.Value.Trim();
                 if (szToken.Length == 0)
                    continue;
                 if (functions.ContainsKey(szToken))
                 {
                     stkOp.Push(szToken);
                     continue;
                 }
                if (!OperatorHelper.IsOperator(szToken))
                {
                    Operand oprnd = OperandHelper.CreateOperand(szToken, varType);
                    oprnd.ExtractAndSetValue(szToken, bFormula);
                    arrFinalExpr.Add(oprnd);

                    szResult += szToken;
                    continue;
                }

                string szOp = szToken;
                if (szOp == "(")
                {
                    stkOp.Push(szOp);
                }
                else if (szOp == ")")
                {
                    string szTop;
                    while ((szTop = (string) stkOp.Pop()) != "(")
                    {
                        IOperator oprtr = OperatorHelper.CreateOperator(szTop);
                        arrFinalExpr.Add(oprtr);

                        szResult += szTop;

                        if (stkOp.Count == 0)
                            throw new RPN_Exception("Unmatched braces!");
                    }
                }
                else
                {
                    if (stkOp.Count == 0 || (string) stkOp.Peek() == "("
                                         || OperatorHelper.IsHigherPrecOperator(szOp, (string) stkOp.Peek()))
                    {
                        stkOp.Push(szOp);
                    }
                    else
                    {
                        while (stkOp.Count != 0)
                            if (OperatorHelper.IsLowerPrecOperator(szOp, (string) stkOp.Peek())
                                || OperatorHelper.IsEqualPrecOperator(szOp, (string) stkOp.Peek()))
                            {
                                string szTop = (string) stkOp.Peek();
                                if (szTop == "(")
                                    break;
                                szTop = (string) stkOp.Pop();

                                IOperator oprtr = OperatorHelper.CreateOperator(szTop);
                                arrFinalExpr.Add(oprtr);

                                szResult += szTop;
                            }
                            else
                            {
                                break;
                            }

                        stkOp.Push(szOp);
                    }
                }
            }

            while (stkOp.Count != 0)
            {
                string szTop = (string) stkOp.Pop();
                if (szTop == "(")
                    throw new RPN_Exception("Unmatched braces");

                IOperator oprtr = OperatorHelper.CreateOperator(szTop);
                arrFinalExpr.Add(oprtr);

                szResult += szTop;
            }

            return arrFinalExpr;
        }

        #endregion
    }
}