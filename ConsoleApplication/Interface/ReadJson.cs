using System;
using System.Collections.Generic;
using ConsoleApplication.Helpers;
using ConsoleApplication.Model;
using System.IO;
using System.Linq;
using ConsoleApplication.Model.ModelSupport;

namespace ConsoleApplication.Interface
{
    public class ReadJson
    {
        public static void ResetConditions()
        {
            Activity.Activities.Clear();
            IntentionalElement.IntentionalElements.Clear();
            Condition.Conditions.Clear();
            Situation.Situations.Clear();
        }



        private readonly string _nameJsonFile;// = @"resources/import.json";
        public ReadJson(string nameJsonFile)
        {
            Activity.Activities.Clear();
            IntentionalElement.IntentionalElements.Clear();
            Condition.Conditions.Clear();
            Situation.Situations.Clear();
            _nameJsonFile = nameJsonFile;
        }
        public void Read()
        {
            
            string text = File.ReadAllText(_nameJsonFile);

            List<string> errors = new List<string>();
            var json = JSON.Parse(text)["conditions"];
            foreach (JSONNode o in json.AsArray)
            {
                string id = o["id"];
                if (IntentionalElement.IntentionalElements.Any(x => x.Id.Equals(id)))
                {
                    errors.Add(id);
                    continue;
                }

                // Condition object auto added to lists by contructor(s)
                // ReSharper disable once ObjectCreationAsStatement
                new Condition(id)
                {
                    Value = 0.0,
                    ContextId = o["Context"],
                    SuperContextId = o["Supercontext"]
                };
            }
            //Condition.Conditions.ForEach(x=>Console.WriteLine(""+x));
            //identities
            json = JSON.Parse(text)["roles"];
            foreach (JSONNode o in json.AsArray)
            {
                string id = o["id"];
                if (IntentionalElement.IntentionalElements.Any(x => x.Id.Equals(id)))
                {
                    errors.Add(id);
                    continue;
                }

                // identity object auto added to lists by contructor(s)
                // ReSharper disable once ObjectCreationAsStatement
                new Identity(id)
                {
                    ContextId = o["Context"],
                    SuperContextId = o["Supercontext"]
                };
            }

            //situations
            json = JSON.Parse(text)["situations"];
            foreach (JSONNode o in json.AsArray)
            {
                string id = o["id"];
                if (IntentionalElement.IntentionalElements.Any(x => x.Id.Equals(id)))
                {
                    errors.Add(id);
                    continue;
                }
                
                // situation object auto added to lists by contructor(s)
                // ReSharper disable once ObjectCreationAsStatement
                new Situation(id)
                {
                    ContextId = o["Context"],
                    SuperContextId = o["Supercontext"]
                };
            }

            foreach (JSONNode o in json.AsArray)
            {
                string id1 = o["id"];
                if (errors.Any(x => x.Equals(id1)))
                    continue;
                Situation node = Situation.Situations.Find(x => x.Id.Equals(id1));
                if (o["parent_situation"] != null)
                {
                    string parentId = o["parent_situation"];
                    Situation parent = Situation.Situations.Find(x => x.Id.Equals(parentId));
                    node.Parent = parent;
                }

                string identityId = o["Role supercontext"];
                Identity identity = Identity.Identities.Find(x => x.Id.Equals(identityId));
                node.Identity1 = identity;
            }

            //activities
            json = JSON.Parse(text)["activities"];
            
            foreach (JSONNode o in json.AsArray)
            {
                string identity = o["Role"];
                string partOf = o["Part of"];
                string type = o["Intentional Element decomposition type"];
                string id = o["id"];
                if (IntentionalElement.IntentionalElements.Any(x => x.Id.Equals(id)))
                {
                    errors.Add(id);
                    continue;
                }
                // activity object auto added to lists by contructor(s)
                // ReSharper disable once ObjectCreationAsStatement
                new Activity(id)
                {
                    Heading = o["Heading nl"],
                    Summary = o["Summary nl"],
                    ContextId = o["Context"],
                    SuperContextId = o["Supercontext"],
                    PartofId = partOf,
                    Situation1 = Situation.Situations.Find(x => x.Id.Equals(o["Context"])),
                    ActivityType = !string.IsNullOrEmpty(type)?(type.Equals("XOR")?TypeActivity.XOR:TypeActivity.IOR):TypeActivity.XOR,
                    Identity1 = !string.IsNullOrEmpty(identity)
                        ? Identity.Identities.Find(x => x.Id.Equals(identity))
                        : null,
                    Order = o["order"].AsInt
                };
            }
            List<Activity> activities = Activity.Activities;
            activities.ForEach(x =>
            {
                //Console.WriteLine("check:"+x.Id+"->"+x.PartofId);
                if (x.PartofId != null)
                {
                    //Console.WriteLine("add supercontext:"+x.Id+":"+x.PartofId);
                    x.SuperContextId = x.PartofId;
                    x.PartOf =  Activity.Activities.Find(y => y.Id.Equals(x.PartofId));
                }
            });

            foreach (JSONNode o in json.AsArray)
            {
                String id1 = o["id"];
                if (errors.Any(x => x.Equals(id1)))
                    continue;
                Activity node = activities.Find(x => x.Id.Equals(id1));
                CreateRelations(node, o["join"], activities, node.Joins);
                CreateRelations(node, o["par"], activities, node.Pars);
                CreateRelations(node, o["seq"], activities, node.Seqs);
                //syncs must NOT be added to relations!
                //syncs indicate that a node must wait for a list of others to be Done before it adds itself to the queue
                CreateSyncs(node, o["sync"], activities);
                CreateContributes(node, o["contributes"], Condition.Conditions);
                //depends = list of Conditions that must be met before the activity can be executed
                CreateDepends(node, o["depend"]);
                JSONNode conditions = o["Dummy element link condition"];
                if (conditions != null)
                {
                    activities.ForEach(x =>
                    {
                        JSONNode rel = conditions[x.Id];
                        if (rel == null) return;
                        // calc unique id for relation (relation is subobject of activity...)
                        string id = "Relation"+RelationCondition.RelationCount+x.Id;
                        //Console.WriteLine("relation:"+x.Id+rel.Value);
                        //Console.WriteLine("relation id:"+id);
                        RelationCondition condition = new RelationCondition(id)
                        {
                            Next = x,
                            Condition = rel.Value,
                            // conditions are assigned the context of the activity (relation is subobject of activity...)
                            ContextId = o["Context"]
                        };
                        node.Conditions.Add(condition);
                    });
                }
            }

            errors.ForEach(x =>
            {
                Console.WriteLine("error; double key! do not add element:"+x);

            });
            IntentionalElement.AssignContextsAndSupercontexts();
            IntentionalElement.CreateRootAndChildren();

        }

        private void CreateDepends(Activity node, JSONNode jsonNode)
        {
            foreach (JSONNode depends in jsonNode.AsArray)
            {
                node.Depends.Add(depends.Value);
            }
        }

        private void CreateContributes(Activity node, JSONNode jsonNode, List<Condition> conditions)
        {
            foreach (JSONNode join in jsonNode.AsArray)
            {
                string id = join["target"].Value;
                string expression = join["expression"].Value;
                //Console.WriteLine("compare id:"+id);  
                Condition f = conditions.Find(x =>
                {
                    if (x.Id == null)
                        return false;
                    //Console.WriteLine("compare id inside:"+x.Id);  
                    return x.Id.Equals(id);
                });
                if (f != null)
                {
                    Contributes c = new Contributes(f, expression);
                    node.Contribute.Add(c);
                }
            }
        }

        private void CreateSyncs(Activity node, JSONNode jsonNode, List<Activity> activities)
        {
            foreach (JSONNode join in jsonNode.AsArray)
            {
                string id = join.Value;
                //Console.WriteLine("compare id:"+id);  
                Activity f = activities.Find(x =>
                {
                    if (x.Id == null)
                        return false;
                    //Console.WriteLine("compare id inside:"+x.Id);  
                    return x.Id.Equals(id);
                });
                if (f != null)
                {
                    f.Syncs.Add(node);
                    node.ToSync.Add(new SyncSemaphore(f));
                }
            }
        }

        private static void CreateRelations(Activity node, JSONNode jsonNode, List<Activity> activities,
            List<Activity> nodeRelations, bool isSuccessor = true, bool log = false)
        {
            foreach (JSONNode join in jsonNode.AsArray)
            {
                string id = join.Value;
                if (log)
                    Console.WriteLine("compare id:" + id);
                Activity f = activities.Find(x =>
                {
                    if (x.Id == null)
                        return false;
                    //Console.WriteLine("compare id inside:"+x.Id);  
                    return x.Id.Equals(id);
                });
                if (log)
                    Console.WriteLine("compare id inside2:"+join.Value);  

                if (f != null)
                {
                    if (log)
                        Console.WriteLine("compare id inside3:"+join.Value);  
                    nodeRelations.Add(f);
                    if (isSuccessor)
                    {
                        f.Predecessors.Add(node);
                        node.Successors.Add(f);
                    }
                }
            }
        }


    }
}