using System.Collections.Generic;

namespace ConsoleApplication.Model
{
    /// <summary>
    /// stores Condition (variable to store the state of the entire model)
    /// </summary>
    public class Condition : IntentionalElement
    {
        public static readonly List<Condition> Conditions = new List<Condition>();


        public Condition(string id) : base(id)
        {
            Condition found = Conditions.Find(y => id.Equals(y.Id));
            if (found!=null)
            {
                Conditions.Remove(found);
            }
            Conditions.Add(this);
        }

    }
}