using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication.Model.ModelSupport;
using ConsoleApplication.Model.Parser;

// ReSharper disable ConvertToAutoProperty

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace ConsoleApplication.Model
{
    /// <summary>
    /// Runningmode; can be: Running, Active, Joining or Suspended
    /// </summary>
    public enum RunningMode
    {
        Running,
        Active,
        Joining,
        Suspended
    }

    public enum TypeActivity
    {
        // Hans has defined these constants in capitals
        // ReSharper disable once InconsistentNaming
        XOR,

        // ReSharper disable once InconsistentNaming
        IOR
    }

    public class EndJoin
    {
        public EndJoin(Thread joinThread, int joinValue)
        {
            JoinThread = joinThread;
            JoinValue = joinValue;
        }
        public Thread JoinThread { get; set; }
        public int JoinValue { get; set; }
        
    }


    public class Activity : IntentionalElement
    {
        public static readonly List<Activity> Activities = new List<Activity>();
        private readonly List<RelationCondition> _conditions = new List<RelationCondition>();
        private readonly List<Activity> _joins = new List<Activity>();
        private readonly List<Activity> _pars = new List<Activity>();
        private readonly List<Activity> _seqs = new List<Activity>();
        private readonly List<Activity> _successors = new List<Activity>();
        private readonly List<Activity> _syncs = new List<Activity>();
        public readonly List<SyncSemaphore> ToSync = new List<SyncSemaphore>();
        public readonly List<string> Depends = new List<string>();
        public readonly List<EndJoin> EndJoins = new List<EndJoin>();
        public Thread CurrentThread { get; set; }

        public readonly List<Activity> Predecessors = new List<Activity>();
        private string _heading;

        private Identity _identity1;

        private RunningMode _mode;
        private int _numJoins;
        private Activity _partOf;
        private Situation _situation1;
        private string _summary;
        private TypeActivity _activityType;

        public void AddJoin(Thread thread)
        {
            EndJoin endJoin = EndJoins.Find(x => x.JoinThread == thread);
            if (endJoin == null)
            {
                endJoin = new EndJoin(thread, TotalJoins);
                EndJoins.Add(endJoin);
            }
            //first join has been passed
            endJoin.JoinValue--;
        }

        public Activity(string id) : base(id)
        {
            Activity found = Activities.Find(y => id.Equals(y.Id));
            if (found != null)
            {
                Activities.Remove(found);
            }

            Activities.Add((this));

            Mode = RunningMode.Suspended;
            NumJoins = 0;
            TotalJoins = 0;
            Done = false;
        }
        //public SyncedMode SyncMode { get; set; }

        public string Summary
        {
            get => _summary;
            set => _summary = value;
        }

        public RunningMode Mode
        {
            get => _mode;
            set => _mode = value;
        }

        public string Heading
        {
            get => _heading;
            set => _heading = value;
        }

        public List<RelationCondition> Conditions => _conditions;

        public List<Activity> Joins => _joins;

        public List<Activity> Pars => _pars;

        public List<Activity> Seqs => _seqs;

        public List<Activity> Syncs => _syncs;

        public List<Activity> Successors => _successors;

        public Situation Situation1
        {
            get => _situation1;
            set => _situation1 = value;
        }

        public Identity Identity1
        {
            get => _identity1;
            set => _identity1 = value;
        }

        public TypeActivity ActivityType
        {
            get => _activityType;
            set => _activityType = value;
        }

        public Activity PartOf
        {
            get => _partOf;
            set => _partOf = value;
        }

        public bool Done { get; private set; }

        public int NumJoins
        {
            get => _numJoins;
            set => _numJoins = value;
        }

        public string PartofId { get; set; }
        public readonly List<Contributes> Contribute = new List<Contributes>();

        //ior-cluster remarks
        //no order in the children defined yet
        //currently IOR-activities can be executed in the following form:
        /*
         {
            "Context": "IKH Colleagues P1S3",
            "Dummy element link condition": {},
            "Heading en": "IKH Colleagues Do something P1S3",
            "Heading nl": "IKH Colleagues Do something P1S3",
            "Intentional Element decomposition type": "IOR",
            "Role": "IKH Colleagues",
            "id": "IKH Colleagues Do something P1S3",
            "is_leaf": false,
            "join": [
                "IKH End P1S3"
            ],
            "par": [],
            "seq": [],
            "type": "Activity"
        },
        {
            "Context": "IKH Colleagues P1S3",
            "Dummy element link condition": {},
            "Heading en": "IKH Colleagues Do something P1S3 sub1",
            "Heading nl": "IKH Colleagues Do something P1S3 sub1",
            "Intentional Element decomposition type": "XOR",
            "Role": "IKH Colleagues",
            "id": "IKH Colleagues Do something P1S3 sub1",
            "is_leaf": true,
            "join": [],
            "par": [],
            "seq": [],
            "Supercontext": "IKH Colleagues Do something P1S3",
            "type": "Activity"
        },{
            "Context": "IKH Colleagues P1S3",
            "Dummy element link condition": {},
            "Heading en": "IKH Colleagues Do something P1S3 sub2",
            "Heading nl": "IKH Colleagues Do something P1S3 sub2",
            "Intentional Element decomposition type": "XOR",
            "Role": "IKH Colleagues",
            "id": "IKH Colleagues Do something P1S3 sub2",
            "is_leaf": true,
            "join": [],
            "par": [],
            "seq": [],
            "Supercontext": "IKH Colleagues Do something P1S3",
            "type": "Activity"
        },
         */

        /// <summary>
        /// check if activity is available to be excuted. Returns the activity if available, or it's direct
        /// IOR-descendant. If not available, the "isAvailable"-flag of the EligibleResult-object is set to "false"
        /// </summary>
        /// <returns></returns>
        public EligibleResult Eligible()
        {
            // a primitive is eligible for execution if:
            // node can only be executed once
            if (Done)
            {
                //Console.WriteLine("already done:"+Id);
                return new EligibleResult(null, true);
            }

            //0 anton: do children-check
            if (Children.Count > 0 && IsPQRCluster())
            {
                Activity actFound = null;
                Children.ForEach(x =>
                {
                    Activity act = (Activity)x;
                    double highest = Double.MinValue;
                    act.Depends.ForEach(y =>
                    {
                        double res = new ExpressionParser(@"" + y).EvaluateDouble();

                        if (res > highest)
                        {
                            highest = res;
                            actFound = act;
                        }
                    });
                    // if (found)
                    //     actFound = act;
                });
                //if it is meant as a comparison of the highest, this could be a solution:
                /*
                double highest = -30;
                Children.ForEach(x =>
                {
                    Activity act = (Activity)x;
                    //found = true;
                    act.Depends.ForEach(y =>
                    {
                        double res = new Guard(@""+y).EvaluateDouble();

                        if (res>highest)
                        {
                            highest = res;
                            actFound = act;
                        }
                    });
                });
                 */
                if (actFound != null)
                {
                    //if depends evaluates true: create temporary connection between parent and child
                    //this emulates single compound-cluster; see if this is sufficient, otherwise must be improved!
                    //Console.WriteLine("depends to true!:" + Id + " --> " + actFound.Id + ":");
                    Successors.Add(actFound);
                    actFound.Successors.Add(this);
                }
            }

            // search in children, for the first eligible Activity.
            if (Children.Count > 0 && !FinishedCluster) //pretend cluster is IOR-relation
            {
                //get child in _successors that is in Children; do this recursively
                Activity childSuccessor = Successors.Find(successor =>
                    Children.Exists(child => successor.Id.Equals(child.Id)));
                // if childSuccessor not there, the cluster is not correct....!!!
                if (childSuccessor == null)
                {
                    //no direct child; pick first one in Children
                    //childSuccessor = (Activity)Children[0];
                    //Console.WriteLine("successors:" + String.Join(",", Successors.Select(x => x.Id)));
                    //Console.WriteLine("children:" + String.Join(",", Children.Select(x => x.Id)));
                    //Console.WriteLine("error:no child successor:" + Id);
                    return new EligibleResult(null, false);
                }

                //if child is not eligible; it will never occur on Threadlist......
                EligibleResult tempResult = childSuccessor.Eligible();
                if (!tempResult.IsAvailable)
                {
                    Algorithm.Algorithm.StaticAlgorithm.AppendActivity(this);
                }

                return tempResult;
            }

            // 1 – in case of a join, all sibling threads in joining (ready to join) state
            EndJoin endJoin = EndJoins.Find(x => x.JoinThread == CurrentThread);
            string threadJoin = "allowed";
            if (endJoin != null)
            {
                if (endJoin.JoinValue == 0)
                {
                    //Console.WriteLine("1"+Id+"join allowed:");
                }
                else
                {
                    threadJoin = "not allowed";
                    //Console.WriteLine("1"+Id+"join not allowed1:"+":"+endJoin.JoinValue+":"+NumJoins);
                    //return new EligibleResult(null, false);
                }
            }
            else
            {
                if (TotalJoins > 0)
                {
                    //Console.WriteLine("1"+Id+"join allowed:");
                    //Console.WriteLine("join not allowed1a:"+Id);//wrong!!
                    //return new EligibleResult(null, false);
                }
                else
                {
                    //Console.WriteLine("1"+Id+"join allowed:");
                }
            }

            string stringJoin = "allowed";
            if (NumJoins > 0)
            {
                stringJoin = "not allowed";
                if (!stringJoin.Equals(threadJoin))
                {
                    //Console.WriteLine(Id+"thread and normal join do not work equal!!!!!");
                }
                //Console.WriteLine("2"+Id+"join not allowed2:"+":"+NumJoins);
                return new EligibleResult(null, false);
            }
            else
            {
                //Console.WriteLine("2"+Id+"join allowed static:"+":"+NumJoins);
            }
            if (!stringJoin.Equals(threadJoin))
            {
                //Console.WriteLine(Id+"thread and normal join do not work equal!!!!!");
            }
            else
            {
                Console.WriteLine(Id+" thread and normal join do work equal");
            }


            // 2 - guard evaluates to true
            // 3 – depends evaluates to true
            bool resDepends = true;
            Depends.ForEach(x =>
            {
                bool res = new ExpressionParser(@"" + x).EvaluateBool();

                if (!res)
                {
                    //depends to false
                    resDepends = false;
                }
            });
            if (!resDepends)
            {
                //return new EligibleResult(null, false);
            }

            // 4 – sync semaphore does not block
            EligibleResult ret = new EligibleResult(this, true);
            Syncs.ForEach(x =>
            {
                // check if sync-int > 0
                SyncSemaphore ss = x.ToSync.Find(y => y.Activity1.Id.Equals(Id));
                if (ss.Value <= 0)
                {
                    //no sync yet; node not executable
                    ret = new EligibleResult(null, false);
                }
            });
            ToSync.ForEach(x =>
            {
                if (x.Value > 0) //some other node has not reacted yet to your sync-signal!
                {
                    //otherwise its Value will be greater than 1! Wait for the other to appear (again)
                    //Console.WriteLine("some other node has not reacted yet to sync-signal:" + Id + ":" +
                    //                  x.Activity1.Id);
                    ret = new EligibleResult(null, false);
                }
            });

            return ret;
        }

        // indicates if IOR-cluster is finished
        // ReSharper disable once InconsistentNaming
        public bool FinishedCluster { get; set; }
        public int TotalJoins { get; set; }

        public RelationCondition GetCondition(Activity relation)
        {
            return Conditions.Find(x => x.Next.Id.Equals(relation.Id));
        }

        /// <summary>
        /// check if activity is head of PQR-cluster
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public bool IsPQRCluster()
        {
            return _activityType == TypeActivity.XOR;
        }

        public string Execute()
            //actually there are two ways of execution:
            //1. a leaf-activity can be executed (leads to changes of conditions etc.)
            //2. a non-leaf-activity can be ACTIVATED. 
            //Hans: The Qs (leaf activities) of a PQR cluster can be executed, only one Q will be executed
        {
            string log = "";

            //for all syncs: decrease sync-int
            //sync is gekoppeld aan een par (bij iedere par een draad) par --> draad
            //history is ook gekoppeld aan draden
            //dus draad (Thread) wordt aparte klasse
            Syncs.ForEach(x =>
            {
                SyncSemaphore ss = x.ToSync.Find(y => y.Activity1.Id.Equals(Id));
                ss.Value--;
            });
            Pars.ForEach(x =>
            {
                x.CurrentThread = new Thread(CurrentThread);
            });
            Joins.ForEach(x =>
            {
                if(x.CurrentThread == null) 
                    x.CurrentThread =  new Thread(CurrentThread.PreviousThread);
            });
            Seqs.ForEach(x =>
            {
                x.CurrentThread = CurrentThread;
            });
            //only change done for executable node
            Done = true;
            //a composite activity cannot be executed
            if (Children.Count == 0)
            {
                //Console.WriteLine("execute:" + Id);
            }
            else
            {
                log += "examine/follow:" + Id;
            }

            //for all to-syncs: increase sync-int
            ToSync.ForEach(x => { x.Value = 1; });

            Contribute.ForEach(x =>
            {
                double res = new ExpressionParser(x.Expression).EvaluateDouble();
                //Console.WriteLine("evaluate:" + x.Target.Id + " --> " + res);
                x.Target.Value = res;
            });
            UpdateValuesOfActivityConditions();
            if (Children.Count > 0 && IsPQRCluster())
            {
                //remove temporary connection between parent and child
                Activity child = Successors.Find(x => Children.Exists(y => y.Id.Equals(x.Id)));
                Successors.Remove(child);
                child.Successors.Remove(this);
            }

            //check if child returns to parent (child is end-node of IOR/tempXOR-cluster)
            Activity nextParent = Successors.Find(parent =>
            {
                return parent.Children.Exists(child => child.Id.Equals(Id));
            });
            if (nextParent == null) return log;
            log += Id + ":set parent finished:" + nextParent.Id;
            nextParent.FinishedCluster = true;
            return log;
        }

        /// <summary>
        /// update values of elements in A-set and r-set of current activity
        /// </summary>
        private void UpdateValuesOfActivityConditions()
        {
            /*
             https://test.projectenportfolio.hz.nl/wiki/index.php/LC_00442
Updating conditions.

1. A = uitgevoerde activiteit
2. A-set = alle Activiteit in PQR-cluster van A (te bereiken via part-of)

A-set-Value := A Value

3. R-set = alle contributes verbonden met A-set
4. Breid R-set uit via part-of relations
R-set-Value = (contributes + part-of)-Value (using +)

5. Alle elementen in R-set:
kijk of ze children hebben (child part-of parent)
Children-Value = R-(parent)-Value

Herhaal dit proces voor de Conditions
(dus 1 t/m 5 wordt 2 x uitgevoerd; voor de Acticties die verboden zijn via: Contributes, 
en voor de Condities die verbonden zijn met de A-set)
             */
            //update conditions for all parents recursively if leaf-activity
            if (Children.Count == 0) //it is leaf
            {
                //initialize Rset = contributes of current activity and all its parents
                //NB Rset includes also conditions, because conditions and activities inherit both from IntentionalElement
                //start with the r-set for the a-set
                List<IntentionalElement> contributesR = SetValueParents(this);
                // create total rSet which includes the rset for the aset as a start
                List<IntentionalElement> rSet = new List<IntentionalElement>();
                rSet.AddRange(contributesR);
                //determine closure of Rset; add parents of members of Rset
                contributesR.FindAll(x1 => x1.GetType() == typeof(Activity))
                    //select activities in list, and cast it to Activity
                    .Select(y => (Activity)y).ToList()
                    //set value to all parents of x in subset, and add these parent to the r-set
                    .ForEach(x => { rSet.AddRange(SetValueParents(x)); });
                //set value for all children of members of Rset for all activities in r-set
                rSet.FindAll(x1 => x1.GetType() == typeof(Activity))
                    .Select(y => (Activity)y).ToList()
                    //it is activity; set children-value
                    .ForEach(x => { SetChildrenValue(x.Children, x.Value); });
            }
        }

        /// <summary>
        /// set parents-value to value of Activity x1
        /// </summary>
        /// <param name="x1">the leaf-activity to start from</param>
        /// <returns>the conditions for the parents as part of the r-set</returns>
        private static List<IntentionalElement> SetValueParents(Activity x1)
        {
            List<IntentionalElement> contributesR = new List<IntentionalElement>();
            contributesR.AddRange(x1.Contribute.Select(x => x.Target));
            Activity p = x1.PartOf;
            //iterate up, following the part-of-property to parent-activity
            while (p != null)
            {
                //add contributes; update it's value as a side-effect
                contributesR.AddRange(p.Contribute.Select(x =>
                {
                    x.Target.Value = x1.Value;
                    //Console.WriteLine("Value of:"+x.Target.Id+":"+x.Target.Value);
                    return x.Target;
                }));
                p.Value = x1.Value;
                //Console.WriteLine("Value of:"+p.Id+":"+p.Value);
                p = p.PartOf;
            }

            return contributesR;
        }

        /// <summary>
        /// set value of all elements of children to it's parent value
        /// </summary>
        /// <param name="children"></param>
        /// <param name="value"></param>
        private void SetChildrenValue(List<IntentionalElement> children, double value)
        {
            children.ForEach(x =>
            {
                x.Value = value;
                if (x.GetType() == typeof(Activity))
                {
                    Activity x1 = (Activity)x;
                    SetChildrenValue(x1.Children, x1.Value);
                }
            });
        }

        public override string ToString()
        {
            return
                $"Activity: {Id}{(Situation1 != null ? "\n- Situation:" + Situation1.Id : "")}" +
                $"{(Identity1 != null ? "\n- Identity:" + Identity1.Id : "")}" +
                $"{(PartOf != null ? "\n- Part of:" + PartOf.Id : "")}" +
                $"{(!string.IsNullOrEmpty(Heading) ? "\n- Heading:" + Heading : "")}" +
                $"{(!string.IsNullOrEmpty(Summary) ? "\n- Summary:" + Summary : "")}" +
                $"{"\n- Type:" + ActivityType}" +
                $"{(Conditions.Count > 1 ? "\n- Conditions:" + GetConditionsAsString() : "")}" +
                GetRelationsAsString(Joins, "Joins") + GetRelationsAsString(Seqs, "Seqs") +
                GetRelationsAsString(Pars, "Pars");
        }

        private string GetConditionsAsString()
        {
            return "[" + string.Join(",", Conditions.Select(x => x.Next.Id + ": " + x.Condition)) + "]";
        }

        private static string GetRelationsAsString(IReadOnlyCollection<Activity> relations, string description)
        {
            return
                $"{(relations.Count > 0 ? "\n- " + description + ":[" + string.Join(",", relations.Select(x => x.Id)) + "]" : "")}";
        }

        /// <summary>
        /// check if activity is head of Compound-cluster
        /// </summary>
        /// <returns></returns>
        public bool IsCompoundCluster()
        {
            return Children.Count > 0;
        }
    }
}