using System.Collections.Generic;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace ConsoleApplication.Model
{
    public class Identity:IntentionalElement
    {
        public static readonly List<Identity> Identities = new List<Identity>();

        public Identity(string id) : base(id)
        {
            Identity found = Identities.Find(y => id.Equals(y.Id));
            if (found!=null)
            {
                Identities.Remove(found);
            }
            Identities.Add(this);
        }

        public override string ToString()
        {
            return "Identity: " + Id;
        }
    }
}