using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using ConsoleApplication.Helpers;

// ReSharper disable InvalidXmlDocComment

namespace ConsoleApplication.Model.Parser
{
    /// <summary>
    /// Parser of logical and numerical expressions
    /// known issues:
    /// 1. only use unary - (like: -2) at start of expression, or after +,-,*,/,>, < ,>=,<=; otherwise use: _
    /// 
    /// example use:
    /// new ExpressionParser(@" multiply(sqr(exp(2)),exp(sqr(0.5 + sqrt(1+0.5)))) + exp ( 2 )").EvaluateDouble()
    /// </summary>
    public class ExpressionParser
    {
        public ExpressionParser(string s)
        {
            Expression = s;
        }

        public ExpressionParser(string s, bool constantParameters)
        {
            Expression = s;
            _constantParameters = constantParameters;
        }

        //in new setup of ExpressionParser these functions are parsed BEFORE the parser is evoked,
        // ReSharper disable once MemberCanBePrivate.Global
        public readonly List<string> BooleanFunctions = new List<string>
            // logical functions are treated specially; must return 0/1, and are surrounded by (.. == 1)
            { "depends", "option", "role", "activity_done", "textfunc" };
        // public static readonly List<string> ConditionSetters = new List<string>
        //     // ConditionSetters functions are treated specially; they do not replace condition-names
        //     { "set_condition"};

        // ReSharper disable once MemberCanBePrivate.Global
        public readonly IDictionary<string, Func<object, object>> Functions =
            new Dictionary<string, Func<object, object>>
            {
                //function parameters are: List<double> or: List<string>
                { "exp", x => Math.Exp((double)((List<object>)x)[0]) },
                { "sqrt", x => Math.Sqrt((double)((List<object>)x)[0]) },
                { "sqr", x => (double)((List<object>)x)[0] * (double)((List<object>)x)[0] },
                { "sin", x => Math.Sin((double)((List<object>)x)[0]) },
                { "cos", x => Math.Cos((double)((List<object>)x)[0]) },
                { "abs", x => Math.Abs((double)((List<object>)x)[0]) },
                //activity-done function to see if an  activity is done yes/no
                {
                    "activity_done", d =>
                    {
                        long val = Convert.ToInt64((double)((List<object>)d)[0]);
                        Activity actDone = Activity.Activities.Find(x => x.IntId == val);
                        // as a workaround the self-defined functions must return 0/1 (true must evaluate to 1)
                        double d1 = actDone.Done ? 1.0 : 0.0;
                        return d1;
                    }
                },
                {
                    "textfunc", (d) =>
                        //example function to show how to deal with strings as parameters in self-defined functions
                    {
                        List<Object> r = (List<Object>)d;
                        Console.WriteLine("parameters:" + r[0] + ":" + r[1]);
                        //deal with first parameter
                        return 1.0;
                    }
                },
                { "role", (str) => 1.0 },
                { "option", (str) => 1.0 },
                {
                    "depends", (str) =>
                    {
                        List<string> r = (List<string>)str;
                        Console.WriteLine(r[0]);
                        return 1.0;
                    }
                },
                //multiple parameters (with separator:",") are allowed
                {
                    "multiply", (o) =>
                    {
                        List<object> p = (List<object>)o;

                        return (double)p[0] * (double)p[1];
                    }
                },
                {
                    "set_condition", (d) =>
                        //set condition-value absolute
                    {
                        List<object> r = (List<object>)d;
                        string targetCondition = (string)r[0];
                        string contributeCondition = (string)r[1];
                        string contributesValue = (string)r[2];
                        double weightOriginal = 0.0;
                        IntentionalElement.UpdateConditionFromContributesCondition(targetCondition, contributeCondition, contributesValue, weightOriginal);
                        //deal with first parameter
                        return 1.0;
                    }
                },
                {
                    //set_condition_fac:valueb += fac * valuea
                    "set_condition_fac", (d) =>
                        //set condition-value fac (valueb +=...)
                    {
                        List<object> r = (List<object>)d;
                        string targetCondition = (string)r[0];
                        string contributeCondition = (string)r[1];
                        string contributesValue = (string)r[2];
                        double weightOriginal = 1.0;
                        IntentionalElement.UpdateConditionFromContributesCondition(targetCondition, contributeCondition, contributesValue, weightOriginal);
                        //deal with first parameter
                        return 1.0;
                    }
                }

            };

        private readonly bool _constantParameters;

        public Activity SyncAct { get; set; }

        // ReSharper disable once MemberCanBePrivate.Global
        public string Expression { get; set; }


        /// <summary>
        /// Evaluate double expression; result is: double
        /// </summary>
        /// <returns>the result for the parsed expression</returns>
        public double EvaluateDouble()
        {
            RPNParser parser = new RPNParser();
            object res = parser.EvaluateExpression(ReplaceConstants(Expression),
                Type.GetType("System.Double"), false, null);
            if (double.TryParse(""+res, out var d)) return d;
            return (bool)res ? 1 : 0;
        }

        /// <summary>
        /// convert double value to string; surrounds the value with brackets for convenience
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private string ValToString(double val)
        {
            bool lessThanZero = val < 0;
            return "(" + (lessThanZero ? "0" : "") + val.ToString(CultureInfo.InvariantCulture) + ")";
        }

        /// <summary>
        /// replace constants of variables with their values
        /// </summary>
        /// <param name="szExpression">expression to change</param>
        /// <returns>cleaned expression (function-calls replaced with their outcome)</returns>
        private string ReplaceConstants(string szExpression)
        {
            //replace string-id's with their int-id's so the parser can deal with parameters of functions
            if (!_constantParameters)
            {
                Activity.Activities.OrderByDescending(x1 => x1.Id.Count()).ToList().ForEach(x =>
                {
                    szExpression = szExpression.Replace(x.Id, x.IntId.ToString());
                });
            }

            Identity.Identities.OrderByDescending(x1 => x1.Id.Count()).ToList().ForEach(x =>
            {
                szExpression = szExpression.Replace(x.Id, x.IntId.ToString());
            });
            if (!_constantParameters)
            {
                Condition.Conditions.OrderByDescending(x1 => x1.Id.Count()).ToList().ForEach(x =>
                {
                    //Console.WriteLine("replace:"+x);
                    szExpression = szExpression.Replace(x.Id, ValToString(x.Value));
                });
            }

            szExpression = szExpression.Replace(" ", "");
            //deal with unary min; it is replaced with _
            if (!_constantParameters)
                new List<string> { "+", "*", "/", "-", ">", "<", ">=", "<=", "==" }.ForEach(x =>
                {
                    szExpression = szExpression.Replace(x + "-", x + "_");
                });

            szExpression = ApplySelfDefinedFunctions(szExpression);

            return szExpression;
        }

        /// <summary>
        /// apply self-defined functions
        /// </summary>
        /// <param name="szExpression">the expression to analyze and change</param>
        /// <returns>changed szExpression</returns>
        private string ApplySelfDefinedFunctions(string szExpression)
        {
            //apply self-defined functions 
            //they are applied before the real parsing starts
            //replace every function-call with their (double...)-result
            bool error = true;
            while (error)
            {
                error = false;
                foreach (KeyValuePair<string, Func<object, object>> entry in Functions)
                {
                    MatchCollection match1 = Regex.Matches(szExpression, @"" + entry.Key + @"\(([^)]*)\)");
                    int i1 = 1;
                    foreach (Match o in match1)
                    {
                        //uneven items give the entire expressions (including before- and end-terms)
                        string replace = "";
                        foreach (Group mg in o.Groups)
                        {
                            if (i1 % 2 != 0) //uneven
                            {
                                //Console.WriteLine("ab" + mg.Value);
                                replace = mg.Value;
                            }
                            else //Even
                                try
                                {
                                    szExpression =
                                        GetFunctionResultAndUpdateExpression(szExpression, entry, mg.Value,
                                            ref replace);
                                }
                                catch (InvalidCastException)
                                {
                                    Console.WriteLine("InvalidCastException:"+replace);
                                    Console.WriteLine("function cannot be evaluated!" + entry.Key + ":" + replace +
                                                      ":do fallback");
                                    // only replace term(s), leave the function-call itself inside in the expression
                                    szExpression = ReplaceTermWithResult(entry.Key, szExpression, replace);

                                    // make it validate once more, using the new terms inside the function-call
                                    error = true;
                                }

                            i1++;
                        }
                    }
                }

                //Console.WriteLine("sz-->" + szExpression);
            }

            return szExpression;
        }

        /// <summary>
        /// get value for function, and replace the function-call in the original expression with the result
        /// </summary>
        /// <param name="szExpression">the expression to be parsed</param>
        /// <param name="function">the function to be applied</param>
        /// <param name="mgValue">the term found by regexp</param>
        /// <param name="replace">the part of expression thatcan be used to identify the substring involved</param>
        /// <returns>the updated expression, or an exception thrown</returns>
        private string GetFunctionResultAndUpdateExpression(string szExpression,
            KeyValuePair<string, Func<object, object>> function, string mgValue,
            ref string replace)
        {
            // get value for function, and replace the function-call in the original expression with the result
            var newValueDouble =
                GetValueForFunction(szExpression, function, mgValue, ref replace);

            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            string newValue = newValueDouble.ToString();
            if (newValueDouble < 0)
            {
                //workaround; negative doubles must be entered like:(0-3))
                newValue = "(0" + newValue + ")";
            }

            if (BooleanFunctions.Exists(y => y.Equals(function.Key)))
            {
                //workaround; booleans must be entered like(.. == 1)
                newValue = "(" + newValue + "==1)";
            }

            //Console.WriteLine("a2:"+szExpression+":"+replace+":"+newValue);
            szExpression = szExpression.Replace(replace, newValue);
            //Console.WriteLine("a3:"+szExpression);
            return szExpression;
        }

        /// <summary>
        /// get value for function
        /// </summary>
        /// <param name="szExpression">total expression containing the function-call</param>
        /// <param name="function">kv-pair of the function to be parsed</param>
        /// <param name="mgValue">the regexp-value found for the term; used as fallback</param>
        /// <param name="replace">the value found to use for identifying the relevant part of the expression</param>
        /// <returns></returns>
        private double GetValueForFunction(string szExpression,
            KeyValuePair<string, Func<object, object>> function, string mgValue, ref string replace)
        {
            //get term(s) after entry.Key+"("
            string term = GetTerm(function.Key, szExpression, replace, out var endBrace);
            //if not valid: fallback to regexp-suggestion-term
            if (!endBrace)
            {
                term = mgValue;
            }
            else
            {
                // parameters correctly parsed; construct new replace-entry
                replace = function.Key + '(' + term + ')';
            }

            //create parts of list that are separated by ",". Only split when brackets-count equal
            List<string> parts = new List<string>();
            string part = "";
            int openBraces = 0;
            int closeBraces = 0;
            foreach(char c in term){
                string addChar = ""+c;
                switch (c)
                {
                    case '(':
                        openBraces++;
                        break;
                    case ')':
                        closeBraces++;
                        break;
                    case ',':
                        if (openBraces == closeBraces)
                        {
                            parts.Add(part);
                            //start new part
                            part = "";
                            //do not add "," to part(s)
                            addChar = "";
                        }
                        break;
                }
                part += addChar;
            }
            // add last part to parts
            parts.Add(part);

            List<object> parameters = parts.Select(y =>
            {
                //get value of double inside, or evaluate the term if not single double
                if (double.TryParse(y, out var d)) return d;
                // try to evaluate part of the parameters
                try
                {
                    d = new ExpressionParser(@"" + y, _constantParameters)
                        .EvaluateDouble();
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("InvalidOperationException:"+y);
                    return y;
                }
                catch (FormatException)
                {
                    Console.WriteLine("FormatException:"+y);
                    return y;
                }

                return (object)d;
            }).ToList();

            return (double)function.Value(parameters);
        }

        /// <summary>
        /// replace string-term with calculated expression
        /// </summary>
        /// <param name="entryKey"> the function-name</param>
        /// <param name="szExpression"> the total expression so far</param>
        /// <param name="replace">the function-call with the first part of the terms to look for</param> 
        /// <returns>the altered szExpression</returns>
        private string ReplaceTermWithResult(string entryKey, string szExpression, string replace)
        {
            string term = GetTerm(entryKey, szExpression, replace, out var endBrace);
            if (endBrace)
            {
                // get value, and replace term with calculated value
                double d = new ExpressionParser(@"" + term, _constantParameters)
                    .EvaluateDouble();
                // ReSharper disable once SpecifyACultureInStringConversionExplicitly
                string s = d.ToString();


                szExpression = szExpression.Replace('(' + term + ')', '(' + s + ')');
            }
            else
            {
                szExpression = "";
            }

            return szExpression;
        }

        /// <summary>
        /// get term finding end-brace 
        /// </summary>
        /// <param name="entryKey">function-id</param>
        /// <param name="szExpression">expression to find term in</param>
        /// <param name="replace">indicates what to look for</param>
        /// <param name="endBrace">true if end-brace found</param>
        /// <returns>term inside braces if endBrace found</returns>
        private static string GetTerm(string entryKey, string szExpression, string replace, out bool endBrace)
        {
            string startExpression = entryKey + "(";
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            if (szExpression.IndexOf(startExpression) < 0)
            {
                endBrace = false;
                return szExpression;
            }

            string term = "";
            // find start-position of term
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            int i = szExpression.IndexOf(replace) + startExpression.Length;
            // find matching closing brace: )
            //found already the first (the start-open-brace)
            int match = 1;
            while (i < szExpression.Length && !(match == 1 && szExpression[i].Equals(')')))
            {
                char currentChar = szExpression[i];
                term += currentChar;
                switch (currentChar)
                {
                    case '(':
                        match++;
                        break;
                    case ')':
                        match--;
                        break;
                }

                i++;
            }

            if (i == szExpression.Length)
                endBrace = false;
            else
                endBrace = match == 1 && szExpression[i].Equals(')');
            return term;
        }

        /// <summary>
        /// evaluate boolean expression
        /// </summary>
        /// <returns>bool result</returns>
        public bool EvaluateBool()
        {
            // ReSharper disable once RedundantAssignment
            string szExpression = @"((2+3)*2<10 || 1!=1) && 2*2==4";
            szExpression = Expression;
            szExpression = ReplaceConstants(szExpression);
            // now expression only contains numbers and function-calls; spaces can be removed
            szExpression = szExpression.Replace(" ", "");

            //Console.WriteLine("expressions:" + szExpression);
            RPNParser parser = new RPNParser();
            object res = parser.EvaluateExpression(szExpression,
                Type.GetType("System.Double"), false, null);
            //Console.WriteLine("res:"+res);

            return (bool)res;
        }
    }
}