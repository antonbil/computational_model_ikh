using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable ConvertToAutoProperty

namespace ConsoleApplication.Model
{
    public class IntentionalElement
    {
        private const double DefaultValue = 0.3;
        public static void UpdateConditionFromContributesCondition(string targetCondition, string contributeCondition,
            string contributesValue, double weightOriginal)
        {
            Console.WriteLine("parameters:" + targetCondition + ":" + contributeCondition + ":" + contributesValue);
            double valA = IntentionalElements.Find(x => x.Id.Equals(contributeCondition)).Value;
            IntentionalElements.ForEach(x =>
            {
                //Console.WriteLine("replace:"+x);
                if (x.Id.Equals(targetCondition))
                {
                    //Console.WriteLine("id found:" + x);
                    Console.WriteLine("id found:" + x.Id + ":" + contributesValue + ":" + valA);
                    x.Update(contributesValue, weightOriginal, valA);
                }
            });
        }

        public static readonly List<IntentionalElement> IntentionalElements = new List<IntentionalElement>();
        private static long _currentId;
        private string _id;
        private long _intId;
        public List<IntentionalElement> Children = new List<IntentionalElement>();
        public int Order { get; set; }

        protected IntentionalElement(string id)
        {
            Id = id;
            _currentId++;
            IntId = _currentId;
            Value = DefaultValue;//default value
            IntentionalElement found = IntentionalElements.Find(y => id.Equals(y.Id));
            if (found!=null)
            {
                IntentionalElements.Remove(found);
            }
            IntentionalElements.Add(this);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public IntentionalElement Context { get; set; }
        public string ContextId { get; set; }
        // ReSharper disable once MemberCanBePrivate.Global
        public IntentionalElement SuperContext { get; set; }
        public string SuperContextId { get; set; }

        public long IntId
        {
            get => _intId;
            private set => _intId = value;
        }

        public string Id
        {
            get => _id;
            private set => _id = value;
        }

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        // ReSharper disable once MemberCanBePrivate.Global
        public static IntentionalElement Root { get; set; }

        public static void CreateRootAndChildren()
        {
            // find all upper Supercontext's, and put them in a list
            List<IntentionalElement> superContexts = new List<IntentionalElement>();
            IntentionalElements.ForEach(x =>
            {
                // iterate to root/parent until no longer possible
                IntentionalElement hRoot = x;
                while (hRoot.SuperContext != null )
                {
                    hRoot = hRoot.SuperContext;
                }

                // add to roots, if not already there
                if (!superContexts.Any(y => hRoot.Id.Equals(y.Id))) superContexts.Add(hRoot);
            });

            // define root and children based on the list superContexts
            IntentionalElement root;
            const string rootId = "Root";
            if (superContexts.Count == 1)
            {
                root = superContexts.First();
            }
            else
            {
                root = new IntentionalElement(rootId);
                superContexts.ForEach(x =>
                {
                    x.SuperContextId = rootId;
                    x.SuperContext = root;
                    x.ContextId = rootId;
                    x.Context = root;
                });
            }

            //tree is now defined; with one root on top
            Root = root;
            
            // now create children based on parents/supercontexts
            IntentionalElements.ForEach(x =>
            {
                x.SuperContext?.Children.Add(x);
            });
            IntentionalElements.ForEach(x =>
            {
               //sort children
               x.Children = x.Children.OrderBy(y => y.Order)
                   .ToList();
            });
        }

        public static void AssignContextsAndSupercontexts()
        {
            // GetContextIfExists function: returns an Intentional Element with ieElementId if it exists
            IntentionalElement GetContextIfExists(IntentionalElement ieElement, string ieElementId)
            {
                if (ieElementId != null)
                {
                    // find and assign supercontext
                    ieElement = IntentionalElements.Find(y => y.Id.Equals(ieElementId)) ??
                        new IntentionalElement(ieElementId);
                }

                return ieElement;
            }

            // assign context and supercontext based on the strings stored previously
            //create temp list based on IntentionalElements
            //necesary because IntentionalElements cannot be modified inside linq-loop
            List<IntentionalElement> ies = new List<IntentionalElement>(IntentionalElements);
            ies.ForEach(x =>
            {
                x.Context = GetContextIfExists(x.Context, x.ContextId);

                x.SuperContext = GetContextIfExists(x.SuperContext, x.SuperContextId) ?? x.Context;

                if (x.Context == null)
                {
                    x.Context = x.SuperContext;
                }
            });

        }
        public double Value { get; set; }

        public override string ToString()
        {
            return
                $"Condition: {Id}\nValue:{Value}" +
                $"{(!string.IsNullOrEmpty(ContextId) ? "\n- Context:" + ContextId : "")}";
        }

        /*
         Because of the contributes’ magnification and the fact that value of an activity’s outcome or a condition can be combined from different sources, 
         the combined value can fall outside the [-1 .. +1] range. For that reason, the following normalization function is applied.
        = 1 - exp(-x) (if x ≥ 0)
        = -(1 - exp(x)) (if x < 0)
         */
        private double Normalize(double x)
        {
            if (x >= 0)
            {
                x = 1 - Math.Exp(-x);
            }
            else
            {
                x = -(1 - Math.Exp(x));
            }

            return x;
        }

        public double Update(string inc, double weightOriginal, double valA)
        {
            double multiply = 0.0;
            switch (inc)
            {
                case "++":
                    multiply = 2;
                    break;
                case "+":
                    multiply = 1;
                    break;
                case "-":
                    multiply = -1;
                    break;
                case "?":
                    multiply = 0;
                    break;
                case "--":
                    multiply = -2;
                    break;
            }

            //Console.WriteLine("calc:"+valA+":"+multiply);
            Value = weightOriginal * Value + valA*multiply;
            Value = Normalize(Value);
            return Value;
        }

    }
}