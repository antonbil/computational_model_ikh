using System.Collections.Generic;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace ConsoleApplication.Model
{
    public class Situation:IntentionalElement
    {
        public static readonly List<Situation> Situations = new List<Situation>();

        public Situation Parent { get; set; }

        public Identity Identity1 { get; set; }

        public Situation(string id): base(id)
        {
            Situation found = Situations.Find(y => id.Equals(y.Id));
            if (found!=null)
            {
                Situations.Remove(found);
            }
            Situations.Add(this);
        }
        public override string ToString()
        {
            return
                $"Situation: {Id}{(Identity1 != null ? "\n- Identity:" + Identity1.Id : "")}"+
                $"{(Parent != null ? "\n- Parent:" + Parent.Id : "")}";
        }
    }
}