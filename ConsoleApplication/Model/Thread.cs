namespace ConsoleApplication.Model
{
    public class Thread
    {
        public Thread PreviousThread { get; set; }

        public Thread(Thread previousThread)
        {
            PreviousThread = previousThread;
        }
    }
}