namespace ConsoleApplication.Model.ModelSupport
{
    /// <summary>
    /// contributes to Condition (changes value of condition using expression)
    /// </summary>
    public class Contributes
    {
        public Contributes(Condition cond, string expression)
        {
            Target = cond;
            Expression = expression;
        }

        public IntentionalElement Target { get; }
        public string Expression { get; set; }

        public override string ToString() => $"({Target.Id}, {Expression})";
    }
}