namespace ConsoleApplication.Model.ModelSupport
{
    /// <summary>
    /// class to store semaphore (Value)
    /// </summary>
    public class SyncSemaphore
    {
        public SyncSemaphore(Activity act)
        {
            Activity1 = act;
            Value = 0;
        }

        public Activity Activity1 { get; }
        public int Value { get; set; }

        public override string ToString() => $"({Activity1.Id}, {Value})";
    }
}