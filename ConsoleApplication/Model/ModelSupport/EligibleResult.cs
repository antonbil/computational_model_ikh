namespace ConsoleApplication.Model.ModelSupport
{
    /// <summary>
    /// helper class to return activity and boolean in one
    /// </summary>
    public class EligibleResult
    {
        public EligibleResult(Activity act, bool isAvailable)
        {
            Activity1 = act;
            IsAvailable = isAvailable;
        }

        public Activity Activity1 { get; }
        public bool IsAvailable { get; }

        public override string ToString() => $"({Activity1.Id}, {IsAvailable})";
    }
}