namespace ConsoleApplication.Model
{
    /// <summary>
    /// stores the link conditions.
    /// In the model of Hans these are stored inside the page as internal objects
    /// to reflect this the RelationCount is used to generate a unique number as part of the key
    /// </summary>
    public class RelationCondition:IntentionalElement
    {
        public static int RelationCount;
        public Activity Next { get; set; }
        public string Condition { get; set; }

        public RelationCondition(string id) : base(id)
        {
            RelationCount++;
        }
    }
}