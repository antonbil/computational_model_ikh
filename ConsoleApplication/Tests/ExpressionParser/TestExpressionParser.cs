using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication.Algorithm;
using ConsoleApplication.Interface;
using ConsoleApplication.Model;
using NUnit.Framework;

namespace ConsoleApplication.Tests.ExpressionParser
{
    //opdelen in unit-testen en functionele testen
    //kapot-testen....
    [TestFixture]
    //run by rmouse-key on class (example: ComputationalModelTests) and select: Run Unit Tests
    //then they become visible in the Unit Tests-tab/window
    public class ComputationalModelTests
    {
        //name of json-file with test-data
        private string _nameJsonFileTest = @"resources/test1.json";

        /// <summary>
        /// find index of activity inside list of logs
        /// </summary>
        /// <param name="logQueue"></param>
        /// <param name="activityId"></param>
        /// <returns></returns>
        int FindIndex(List<LogActivity> logQueue, string activityId)
        {
            return logQueue.FindIndex(x => x.Activity.Id.Equals(activityId));
        }

        [Theory]
        public void ReadJson()
        {
            new ReadJson(_nameJsonFileTest).Read();
            Assert.AreEqual(88, Activity.Activities.Count);
        }

        [Theory]
        public void Check_Execution_Order_Sync()
        {
            List<LogActivity> logQueueLocal = GetSimpleLogQueue();
            //check sync "IKH Employer Engage 1 step 1 P1S2" --> "IKH PwC Engage 1 step 2 P1S2"
            //Condition button1P1S2 must be set to at least 0.5
            Activity.Activities.ForEach(x =>
            {
                x.Syncs.ForEach(y =>
                {
                    int findIndexY = FindIndex(logQueueLocal, y.Id);
                    int findIndexX = FindIndex(logQueueLocal, x.Id);
                    if (findIndexX >= 0) // x is executed; so must be synced to y
                        Assert.Less(findIndexY, findIndexX, y.Id + " not executed/synced before " + x.Id);
                });
            });
        }

        [Theory]
        public void Check_Execution_Order_Join()
        {
            List<LogActivity> logQueueLocal = GetSimpleLogQueue();

            Activity.Activities.ForEach(x =>
            {
                x.Joins.ForEach(y =>
                {
                    int findIndexY = FindIndex(logQueueLocal, y.Id);
                    if (findIndexY >= 0)
                    {
                        //find all joins that point to y
                        List<Activity> pjoins = Activity.Activities.FindAll(z => z.Joins.Contains(y));
                        pjoins.ForEach(z =>
                        {
                            //Console.WriteLine("check join:"+z.Id+" : "+y.Id);
                            Assert.LessOrEqual(FindIndex(logQueueLocal, z.Id), findIndexY,
                                z.Id + " not joined before " + y.Id);
                        });
                    }
                });
            });
        }

        [Theory]
        public void Check_Execution_Order_Parent_IOR_Executed_After_Child()
        {
            List<LogActivity> logQueueLocal = GetSimpleLogQueue();
            //check "IKH PwC Engage 1 step 2 P1S2" --> "IKH PwC Interact P1S2"
            //Condition button1P1S2 must be set to at least 0.5
            Assert.Less(FindIndex(logQueueLocal, "IKH PwC Engage 1 step 2 P1S2"),
                FindIndex(logQueueLocal, "IKH PwC Interact P1S2"));
        }

        [Theory]
        public void Check_Execution_Order_Button2_On()
        {
            new ReadJson(_nameJsonFileTest).Read();
            //set conditions "IKH Button 1 P1S2" so that first canNOT be passed
            Condition button1P1S2 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 1 P1S2"));
            button1P1S2.Value = -0.5;
            //set conditions "IKH Button 2 P1S2" so that second can be passed
            Condition button2P1S2 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 2 P1S2"));
            button2P1S2.Value = 0.5;

            Algorithm.Algorithm a = new Algorithm.Algorithm();
            a.Execute("IKH Start P1S1");
            List<LogActivity> logQueueLocal = a.LogQueue;
            //check sync "IKH Employer Engage 1 step 1 P1S2" --> "IKH PwC Engage 1 step 2 P1S2"
            Assert.Greater(FindIndex(logQueueLocal, "IKH PwC Engage 2 P1S2"), 0);
            Assert.Greater(FindIndex(logQueueLocal, "IKH Employer Engage 2 P1S2"), 0);
        }

        [Theory]
        public void Test_Set_Condition_Plus()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.5
            };
            // ReSharper disable once UnusedVariable
            Condition fear = new Condition("fear")
            {
                Value = 0.5
            };

            //ConditionSetters.Exists(x => szExpression.Contains(x))
            new Model.Parser.ExpressionParser(@"set_condition(anger,fear,++)", true).EvaluateDouble();

            Assert.Greater(anger.Value, 0.63);
        }

        [Theory]
        public void Test_Set_Condition_Less_Than_One()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.5
            };
            Condition fear = new Condition("fear")
            {
                Value = 0.5
            };

            for (int i = 0; i < 100; i++)
            {
                anger.Update("++", 1, fear.Value);
            }

            Assert.LessOrEqual(anger.Value, 1);
        }

        [Theory]
        public void Test_Set_Condition_For_Activities()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Activity anger = new Activity("anger")
            {
                Value = 0.5
            };
            Activity fear = new Activity("fear")
            {
                Value = 0.5
            };

            //ConditionSetters.Exists(x => szExpression.Contains(x))
            new Model.Parser.ExpressionParser(@"set_condition(anger,fear,++)", true).EvaluateDouble();

            Assert.Greater(anger.Value, fear.Value);
        }

        [Theory]
        public void Test_Set_Condition_Plus_Fac()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.3
            };
            // ReSharper disable once UnusedVariable
            Condition fear = new Condition("fear")
            {
                Value = 0.5
            };

            //set_condition_fac:valueb += fac * valuea
            new Model.Parser.ExpressionParser(@"set_condition_fac(anger,fear,++)", true).EvaluateDouble();

            Assert.GreaterOrEqual(anger.Value, 0.72);
        }

        [Theory]
        public void Test_Set_Condition_Zero()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.5
            };

            // ReSharper disable once UnusedVariable
            Condition fear = new Condition("fear")
            {
                Value = 0.5
            };
            //ConditionSetters.Exists(x => szExpression.Contains(x))
            new Model.Parser.ExpressionParser(@"set_condition(anger,fear,?)", true).EvaluateDouble();

            Assert.AreEqual(anger.Value, 0);
        }

        [Theory]
        public void Test_Set_Condition_Min()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.5
            };

            Condition fear = new Condition("fear")
            {
                Value = 0.5
            };
            new Model.Parser.ExpressionParser(@"set_condition(anger,fear,--)", true).EvaluateDouble();

            Assert.Less(anger.Value, -fear.Value);
        }

        [Theory]
        public void Test_Set_Condition_Min_Less_zero()
        {
            Interface.ReadJson.ResetConditions();
            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = -0.5
            };

            Condition fear = new Condition("fear")
            {
                Value = -0.5
            };
            new Model.Parser.ExpressionParser(@"set_condition(anger,fear,--)", true).EvaluateDouble();

            Assert.Greater(anger.Value, -fear.Value);
        }

        // a PQR activity: precisely one Q can be chosen out of a number of alternative Qs, only one Q will be executed
        [Theory]
        public void Check_Execution_Only_One_Q_Can_Be_Executed_For_P_In_PQR_Cluster()
        {
            List<LogActivity> logQueueLocal = GetSimpleLogQueue();
            Activity.Activities.FindAll(x => x.ActivityType == TypeActivity.XOR && x.Children.Count > 0).ForEach(y =>
            {
                Assert.Less(logQueueLocal.FindAll(z => y.Children.Contains(z.Activity)).Count, 2);
            });
        }

        // inside Compound-cluster: of all seq-relations of a node only one can be executed at a time
        [Theory]
        public void Check_Execution_Only_One_Seq_Can_Be_Executed_For_Node_In_Compound_Cluster()
        {
            List<LogActivity> logQueueLocal = GetSimpleLogQueue();
            Activity.Activities.FindAll(x => x.Seqs.Count > 0).ForEach(yHasSeqs =>
            {
                //select struct with: (log-item,index) for all seqs of y in logQueue
                var structSeqs = logQueueLocal.Select((x1,i)=>new { logSeq = x1, indexSeq = i }).
                    Where(z => yHasSeqs.Seqs.Contains(z.logSeq.Activity)).ToList();
                bool close = false;
                structSeqs.ForEach(l1 =>
                {
                    structSeqs.ForEach(l2=>
                    {
                        if (l1.indexSeq != l2.indexSeq && Math.Abs(l1.indexSeq - l2.indexSeq) < 3)
                        {
                            Console.WriteLine("suspicious loop:"+
                                              l1.logSeq.Activity.Id+":"+l1.indexSeq+":"+l2.logSeq.Activity.Id+":"+l2.indexSeq);
                            close = true;
                        }
                    });
                });
                Assert.False(close);
            });
        }
        // check for an executable path inside the children of a compound cluster
        [Theory]
        public void Check_Executable_Path_In_Compound_Cluster()
        {
            new ReadJson(_nameJsonFileTest).Read();
            Activity.Activities.FindAll(x => x.ActivityType == TypeActivity.IOR && x.Children.Count > 0).ForEach(y =>
            {
                //get list of all children that have successor that point to parent
                List<Activity> children = y.Children.FindAll(z => ((Activity)z).Successors.Contains(y))
                    .Select(z => (Activity)z).ToList();
                int i = 0;
                while (!children.Contains(y) && children.Count > 0 && i < 100)
                {
                    //get all activities that have one of these children in their successors; assign to children (new)
                    children = Activity.Activities.FindAll(pred => children.Exists(child =>
                        pred.Successors.Exists(succ => succ.Id.Equals(child.Id)))).ToList();
                    i++;
                }

                Assert.False(i == 100 || children.Count == 0);
            });
        }

        private List<LogActivity> GetSimpleLogQueue()
        {
            new ReadJson(_nameJsonFileTest).Read();
            //set conditions "IKH Button 1 P1S2" so that first can be passed
            Condition button1P1S2 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 1 P1S2"));
            button1P1S2.Value = 0.5;
            Condition button2P1S2 = Condition.Conditions.Find(x => x.Id.Equals("IKH Button 2 P1S2"));
            button2P1S2.Value = -0.5;

            Algorithm.Algorithm a = new Algorithm.Algorithm();
            a.Execute("IKH Start P1S1");
            List<LogActivity> logQueueLocal = a.LogQueue;
            return logQueueLocal;
        }
    }

    public class ExpressionParserTests
    {
        [Test]
        public void Test_Complicated_Boolean_Comparison()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"((2+3)*2<10 || 2!=1) && 2*2==4").EvaluateBool());
        }

        [Test]
        public void Test_Complicated_Boolean_Comparison_With_Function_Call_Inside()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"multiply(6-(8-sqr(6-4)),sqr(1+(3-2)))>5").EvaluateBool());
        }

        [Test]
        public void Test_Complicated_Boolean_Comparison_With_Function_That_Accepts_Text_Parameters()
        {
            Assert.False(new Model.Parser.ExpressionParser(@"textfunc(a,b)&& 4>5").EvaluateBool());
        }

        [Test]
        public void Test_Textfunction_With_Some_Parameters_Double()
        {
            Assert.False(new Model.Parser.ExpressionParser(@"textfunc((3+2)*4,8)&& 4>5").EvaluateBool());
        }

        [Test]
        public void Test_Complicated_Boolean_Comparison_With_Conditions()
        {
            ReadJson.ResetConditions();

            // ReSharper disable once UnusedVariable
            Condition anger = new Condition("anger")
            {
                Value = 0.5
            };
            // ReSharper disable once UnusedVariable
            Condition happiness = new Condition("happiness")
            {
                Value = 0.3
            };
            // ReSharper disable once UnusedVariable
            Condition motivation = new Condition("motivation")
            {
                Value = 0.2
            };
            Assert.True(new Model.Parser.ExpressionParser(@"(anger -2*(4+sqr(1+2)))<happiness").EvaluateBool());
        }

        [Test]
        public void Test_Double_Function_With_Exp()
        {
            Assert.AreEqual(25.0855369231877, (new Model.Parser.ExpressionParser(@"(2+3)+ exp(3.0)").EvaluateDouble()));
        }

        [Test]
        public void Test_Double_Function_With_Function_Call_Inside_Other_Function_Call()
        {
            Assert.AreEqual(3364,
                new Model.Parser.ExpressionParser(@" sqr(multiply(4 + sqr(0-5), 2))").EvaluateDouble());
        }

        [Test]
        public void Test_Double_Function_With_Function_Call_Inside_Other_Function_Call_2()
        {
            Assert.AreEqual(32, new Model.Parser.ExpressionParser(@" sqr(5-multiply(3,1))*(8)").EvaluateDouble());
        }

        [Test]
        public void Test_Double_Function_With_Complicated_Parameters()
        {
            Assert.AreEqual(1, new Model.Parser.ExpressionParser(@" sqr(4 + (0-5))").EvaluateDouble());
        }

        [Test]
        public void Test_Double_Function_With_Function_Call_Inside_Other_Function_Call_3()
        {
            Assert.That(new Model.Parser.ExpressionParser(@"multiply(sqr(exp(2)),exp(sqr(0.5 + sqrt(1+0.5)))) + exp ( 2 )")
                .EvaluateDouble(), Is.EqualTo(1076.6734).Within(0.1)); // "Expected: 1076.6734 +/- 0.1"
        }

        [Test]
        public void Test_Multiple_2Parameter_Functions()
        {
            Assert.AreEqual(12,
                new Model.Parser.ExpressionParser(@"multiply(2,multiply(2,3))")
                    .EvaluateDouble());
        }

        [Test]
        public void Test_ComplicatedFunction_Multiple_2Parameter_Functions()
        {
            Assert.AreEqual(4,
                new Model.Parser.ExpressionParser(@"multiply(2,multiply(2,3) + 2*(sqr(2) - multiply(2,3)))")
                    .EvaluateDouble());
            //2 *(6 + 2*(4-6)) = 2*(6-4) = 4
        }

        [Test]
        public void Test_ComplicatedFunction_Multiple_2Parameter_Functions2()
        {
            Assert.AreEqual(-48,
                new Model.Parser.ExpressionParser(@"multiply(2+multiply(2,3) + 2*(sqr(2)),3-multiply(2,3))")
                    .EvaluateDouble());
            //(2 + 6+8)*(3-6) = -48
        }

        [Test]
        public void Testminus11()
        {
            Assert.AreEqual(-12,
                new Model.Parser.ExpressionParser(@"4*(2+_5)").EvaluateDouble());
        }

        [Test]
        public void Testminus10()
        {
            Assert.AreEqual(12,
                new Model.Parser.ExpressionParser(@"_4*(2+_5)").EvaluateDouble());
        }

        [Test]
        public void Testminus9()
        {
            //-4 only works if at start of expression
            Assert.AreEqual(12,
                new Model.Parser.ExpressionParser(@"-4*(2+_5)").EvaluateDouble());
        }

        [Test]
        public void Testminus8()
        {
            Assert.AreEqual(9,
                new Model.Parser.ExpressionParser(@"sqr(-2+5)").EvaluateDouble());
        }

        [Test]
        public void Testminus7()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"-2 < 1 || 4>3").EvaluateBool());
        }

        [Test]
        public void Testminus6()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"-2 < _3 || 4>3").EvaluateBool());
        }

        [Test]
        public void Testminus5()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"-2 < 3 || 4>_5").EvaluateBool());
        }

        [Test]
        public void Test_Double_Function_With_Min_At_Start()
        {
            Assert.True(new Model.Parser.ExpressionParser(@"-2 < (0-3) || 4>3").EvaluateBool());
        }

        [Test]
        public void Test_Double_Function_With_Min_At_Start_Of_Expressions()
        {
            //- now works on several places
            Assert.True(new Model.Parser.ExpressionParser(@"-2 < -3 || 4>3").EvaluateBool());
        }

        [Test]
        public void Testminus2()
        {
            //- now works on several places
            Assert.AreEqual(
                new Model.Parser.ExpressionParser(@"sqr(2+-5)").EvaluateDouble(), 9);
        }

        [Test]
        public void Testminus3()
        {
            //- now works on several places
            Assert.True(new Model.Parser.ExpressionParser(@"-3 < -3--1 || 3>4").EvaluateBool());
        }

        [Test]
        public void Testminus4()
        {
            //- now works on several places
            Assert.AreEqual(new Model.Parser.ExpressionParser(@"sqr(3* -3)").EvaluateDouble(), 81);
        }
    }
}